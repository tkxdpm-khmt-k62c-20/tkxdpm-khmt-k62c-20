package app.admin;

import javax.swing.JFrame;
import javax.swing.JPanel;

import app.admin.modules.managebike.AdminManageBikeController;


public class AdminController {
	public final Integer DEFAULT_SCREEN_WIDTH = 1000;
	public final Integer DEFAULT_SCREEN_HEIGHT = 600;
	private static AdminController instance = null;
	private AdminHome homePage;
	private AdminManageBikeController manageBikeController;
	private JFrame adminManageBikePage;
	public synchronized static AdminController getInstance() {
		if (instance == null) {
			instance = new AdminController();
		}
		return instance;
	}
	private AdminController() {
		this.homePage = new AdminHome();
		this.manageBikeController = new AdminManageBikeController();
		this.adminManageBikePage = new JFrame();
		this.adminManageBikePage.setBounds(100, 100, this.DEFAULT_SCREEN_WIDTH, this.DEFAULT_SCREEN_HEIGHT);
		this.adminManageBikePage.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.homePage.setBounds(100, 100, this.DEFAULT_SCREEN_WIDTH, this.DEFAULT_SCREEN_HEIGHT);
		this.adminManageBikePage.add(this.manageBikeController.getDataPagePane());
	}
	
	
	public void displayAdminHome() {
		this.adminManageBikePage.setVisible(false);
		this.homePage.setVisible(true);
		
	}
	public void manageBike() {
		this.homePage.setVisible(false);
		this.adminManageBikePage.setVisible(true);
	}
	
	public void manageStation() {
		//this.homePage.setVisible(false);
		System.out.println("Unavaiable function");
		// TODO load manage station screen 
	}
}
