package app.admin.modules.managebike;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import javax.swing.border.EmptyBorder;

import abstractdata.gui.ADataListPane;
import abstractdata.gui.ADataPagePane;
import abstractdata.gui.ADataSearchPane;
import app.admin.AdminController;
import bean.Vehicle;
import javax.swing.JLabel;
import java.awt.Font;

public class AdminManageBikePage extends ADataPagePane<Vehicle>{

	AdminManageBikeController controller;
	JButton btnAddBike;
	
	public AdminManageBikePage(ADataSearchPane searchPane,ADataListPane<Vehicle> listPane) {
		SpringLayout layout = new SpringLayout();
		this.setLayout(layout);
		this.add(searchPane);
		this.add(listPane);
		btnAddBike = new JButton("Add bike");
		this.add(btnAddBike);
		
		JLabel lblTitle = new JLabel("Manage Bike");
		lblTitle.setFont(new Font("Tahoma", Font.PLAIN, 14));
		add(lblTitle);
		
		layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, lblTitle, 0, SpringLayout.HORIZONTAL_CENTER, this);
		layout.putConstraint(SpringLayout.NORTH, lblTitle, 5, SpringLayout.NORTH, this);
		
		layout.putConstraint(SpringLayout.WEST, searchPane, 5, SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.NORTH, searchPane, 5, SpringLayout.SOUTH, lblTitle);
		layout.putConstraint(SpringLayout.EAST, searchPane, -5, SpringLayout.EAST, this);

		layout.putConstraint(SpringLayout.WEST, listPane, 5, SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.NORTH, listPane, 5, SpringLayout.SOUTH, searchPane);
		layout.putConstraint(SpringLayout.EAST, listPane, -5, SpringLayout.EAST, this);
		layout.putConstraint(SpringLayout.SOUTH, listPane, -5, SpringLayout.NORTH, btnAddBike);
		
		layout.putConstraint(SpringLayout.SOUTH, btnAddBike, -5, SpringLayout.SOUTH, this);
		layout.putConstraint(SpringLayout.HORIZONTAL_CENTER,btnAddBike,0,SpringLayout.HORIZONTAL_CENTER,this);
		
		JButton btnBack = new JButton("Back");
		layout.putConstraint(SpringLayout.NORTH, btnBack, 0, SpringLayout.NORTH, this);
		layout.putConstraint(SpringLayout.WEST, btnBack, 5, SpringLayout.WEST, this);
		add(btnBack);
		
		JButton btnNewButton = new JButton("Manage Active Bike");
		layout.putConstraint(SpringLayout.SOUTH, btnNewButton, 0, SpringLayout.SOUTH, lblTitle);
		layout.putConstraint(SpringLayout.EAST, btnNewButton, 0, SpringLayout.EAST, this);
		add(btnNewButton);
		
		btnBack.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				AdminController.getInstance().displayAdminHome();
			}
		});

	}
	
	public void setController(AdminManageBikeController adminManageBikeController) {
		this.controller = adminManageBikeController;
		this.btnAddBike.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				AdminManageBikePage.this.controller.onBtnAddBikeClick();
			}
		});
	}
}
