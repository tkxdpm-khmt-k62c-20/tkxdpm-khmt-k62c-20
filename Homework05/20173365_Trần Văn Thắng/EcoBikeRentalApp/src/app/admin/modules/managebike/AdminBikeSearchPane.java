package app.admin.modules.managebike;

import javax.swing.JPanel;
import javax.swing.JTextField;

import abstractdata.controller.IDataSearchController;
import abstractdata.gui.ADataSearchPane;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.Map;
import java.awt.GridBagLayout;
import javax.swing.JSpinner;
import javax.swing.SpinnerListModel;

import java.awt.Dimension;
import java.awt.Font;
import javax.swing.SwingConstants;

public class AdminBikeSearchPane extends ADataSearchPane {

	enum BikeType {
		BIKE("bike"), E_BIKE("ebike"), TWIN_BIKE("twinbike");

		String value;

		BikeType(String value) {
			this.value = value;
		}
	}

	private JTextField nameField;
	private JTextField stationField;
	private JTextField idField;
	private JSpinner spType;

	public AdminBikeSearchPane() {
		super();
	}

	@Override
	public void buildControls() {
		// TODO Auto-generated method stub
		GridBagLayout gridBagLayout = (GridBagLayout) getLayout();
		gridBagLayout.columnWeights = new double[] { 0.0, 1.0, 0.0, 1.0, 1.0 };

		JLabel lblNewLabel = new JLabel("Name");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 12));
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 0;
		add(lblNewLabel, gbc_lblNewLabel);

		nameField = new JTextField(20);
		nameField.setEditable(true);
		GridBagConstraints gbc_nameField = new GridBagConstraints();
		gbc_nameField.insets = new Insets(0, 0, 5, 5);
		gbc_nameField.fill = GridBagConstraints.HORIZONTAL;
		gbc_nameField.gridx = 1;
		gbc_nameField.gridy = 0;
		add(nameField, gbc_nameField);
		nameField.setColumns(10);

		JLabel lblNewLabel_3 = new JLabel("id");
		lblNewLabel_3.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_lblNewLabel_3 = new GridBagConstraints();
		gbc_lblNewLabel_3.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_3.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_3.gridx = 2;
		gbc_lblNewLabel_3.gridy = 0;
		add(lblNewLabel_3, gbc_lblNewLabel_3);

		idField = new JTextField();
		GridBagConstraints gbc_idField = new GridBagConstraints();
		gbc_idField.insets = new Insets(0, 0, 5, 5);
		gbc_idField.fill = GridBagConstraints.HORIZONTAL;
		gbc_idField.gridx = 3;
		gbc_idField.gridy = 0;
		add(idField, gbc_idField);
		idField.setColumns(10);

		JLabel lblNewLabel_1 = new JLabel("Type");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.gridx = 0;
		gbc_lblNewLabel_1.gridy = 1;
		add(lblNewLabel_1, gbc_lblNewLabel_1);

		spType = new JSpinner();
		spType.setFont(new Font("Tahoma", Font.PLAIN, 12));
		spType.setPreferredSize(new Dimension(100, 20));
		spType.setModel(new SpinnerListModel(new String[] { "All", "Bike", "EBike", "Twin Bike" }));
		GridBagConstraints gbc_spType = new GridBagConstraints();
		gbc_spType.insets = new Insets(0, 0, 5, 5);
		gbc_spType.gridx = 1;
		gbc_spType.gridy = 1;
		add(spType, gbc_spType);

		JLabel lblNewLabel_2 = new JLabel("Station");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 12));
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 0, 5);
		gbc_lblNewLabel_2.gridx = 2;
		gbc_lblNewLabel_2.gridy = 1;
		add(lblNewLabel_2, gbc_lblNewLabel_2);

		stationField = new JTextField();
		GridBagConstraints gbc_stationField = new GridBagConstraints();
		gbc_stationField.insets = new Insets(0, 0, 0, 5);
		gbc_stationField.fill = GridBagConstraints.HORIZONTAL;
		gbc_stationField.gridx = 3;
		gbc_stationField.gridy = 1;
		add(stationField, gbc_stationField);
		stationField.setColumns(10);

	}

	@Override
	public Map<String, String> getQueryParams() {
		Map<String, String> searchParams = super.getQueryParams();
		if (!nameField.getText().trim().equals("")) {
			System.out.println("name : " + nameField.getText());
			searchParams.put("name", nameField.getText().trim());
		}
		if (!idField.getText().trim().equals("")) {
			System.out.println("id : " + idField.getText());
			searchParams.put("id", idField.getText().trim());
		}
		if (!stationField.getText().trim().equals("")) {
			System.out.println("station : " + stationField.getText());
			searchParams.put("stationId", stationField.getText().trim());
		}

		String typeParam = null;
		String type = spType.getValue().toString();
		if (type.equals("Bike")) {
			typeParam = BikeType.BIKE.value;
		}
		else if (type.equals("EBike")) {
			typeParam = BikeType.E_BIKE.value;
		}
		else if (type.equals("Twin Bike")) {
			typeParam = BikeType.TWIN_BIKE.value;
		}

		if (typeParam != null) {
			searchParams.put("type", typeParam);
		}
		System.out.println("Search : "+searchParams.toString());
		return searchParams;
	}

}
