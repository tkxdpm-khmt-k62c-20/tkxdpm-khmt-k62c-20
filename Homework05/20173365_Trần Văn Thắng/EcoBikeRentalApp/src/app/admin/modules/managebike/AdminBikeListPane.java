package app.admin.modules.managebike;

import javax.swing.BorderFactory;

import abstractdata.controller.ADataPageController;
import abstractdata.gui.ADataListPane;
import abstractdata.gui.ADataSinglePane;
import api.ServerAPI;
import bean.*;

public class AdminBikeListPane extends ADataListPane<Vehicle>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AdminBikeListPane() {
		super();
	}
	public AdminBikeListPane(ADataPageController<Vehicle> controller ) {
		this.controller = controller;
	}
	@Override
	public void decorateSinglePane(ADataSinglePane<Vehicle> singlePane) {
		singlePane.setBorder(BorderFactory.createRaisedBevelBorder());
	}

}
