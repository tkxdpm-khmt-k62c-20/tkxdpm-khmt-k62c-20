package app.admin.modules.managebike;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.text.NumberFormatter;


import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.text.ParseException;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JSpinner;
import javax.swing.SpinnerListModel;
import javax.swing.SpinnerDateModel;
import java.util.Date;
import java.util.HashMap;
import java.util.Calendar;
import javax.swing.JFormattedTextField;
import java.awt.Font;
import java.awt.Color;
import javax.swing.SwingConstants;

public class AddBikePopUp extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField idField;
	private JTextField producerField;
	private JTextField nameField;
	private JTextField stationField;
	private JFormattedTextField weightField;
	private JSpinner spType;
	private JSpinner spDate;
	private JFormattedTextField costField;
	private AdminManageBikeController controller;
	private JTextField licencePlateField;
	private JLabel lbError;

	private HashMap<String,String> getInput() throws ParseException {
		HashMap<String,String> input = new HashMap<String,String>();
		
		Number number;
		
		input.put("id",idField.getText().trim());
		input.put("name",nameField.getText().trim());
		
		input.put("produrcer",producerField.getText().trim());
		input.put("stationId",stationField.getText().trim());
		if (!weightField.getText().trim().equals("")) {
			number = NumberFormat.getInstance().parse( weightField.getText().trim());
			input.put("weight",number.toString());
		}
		if (!costField.getText().trim().equals("")) {
			number = NumberFormat.getInstance().parse(costField.getText().trim());
			input.put("cost",number.toString());
		}
		input.put("licencePlate",licencePlateField.getText().trim());
		input.put("manufacturingDate", spDate.getValue().toString());
		switch (spType.getValue().toString()) {
		case "Bike":
			input.put("type", "bike");
			break;
		case "EBike":
			input.put("type", "ebike");
			break;
		case "Twin Bike":
			input.put("type", "twinbike");
			break;
		}
		
		return input;
	}
	
	/**
	 * Create the dialog.
	 */
	public AddBikePopUp() {
		setFont(new Font("Dialog", Font.PLAIN, 14));
		setTitle("Add Bike");
		getContentPane().setBackground(new Color(51, 102, 255));
		setBackground(new Color(0, 153, 255));
		setForeground(new Color(102, 204, 255));
		setModalExclusionType(ModalExclusionType.APPLICATION_EXCLUDE);
		setResizable(false);
		getContentPane().setFont(new Font("Tahoma", Font.PLAIN, 12));
		setType(Type.POPUP);
		setAlwaysOnTop(true);
		setBounds(100, 100, 615, 298);
		BorderLayout borderLayout = new BorderLayout();
		borderLayout.setVgap(10);
		getContentPane().setLayout(borderLayout);
		contentPanel.setBackground(new Color(51, 102, 255));
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.NORTH);
		contentPanel.setLayout(new GridLayout(0, 4, 5, 10));
		
	    NumberFormat format = NumberFormat.getInstance();
	    NumberFormatter formatter = new NumberFormatter(format);
	    formatter.setValueClass(Integer.class);
	    formatter.setMinimum(0);
	    formatter.setMaximum(Integer.MAX_VALUE);
	    formatter.setAllowsInvalid(true);
		{
			JLabel lblNewLabel = new JLabel("ID");
			lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
			contentPanel.add(lblNewLabel);
		}
		{
			idField = new JTextField();
			idField.setFont(new Font("Tahoma", Font.PLAIN, 14));
			contentPanel.add(idField);
			idField.setColumns(10);
		}
		{
			JLabel lblProducer = new JLabel("Producer");
			lblProducer.setFont(new Font("Tahoma", Font.PLAIN, 14));
			contentPanel.add(lblProducer);
		}
		{
			producerField = new JTextField();
			producerField.setFont(new Font("Tahoma", Font.PLAIN, 14));
			contentPanel.add(producerField);
			producerField.setColumns(10);
		}
		{
			JLabel lblNewLabel_1 = new JLabel("Name");
			lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
			contentPanel.add(lblNewLabel_1);
		}
		{
			nameField = new JTextField();
			nameField.setFont(new Font("Tahoma", Font.PLAIN, 14));
			contentPanel.add(nameField);
			nameField.setColumns(10);
		}
		{
			JLabel lblNewLabel_2 = new JLabel("weight");
			lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
			contentPanel.add(lblNewLabel_2);
		}
		{
			weightField = new JFormattedTextField(formatter);
			weightField.setFont(new Font("Tahoma", Font.PLAIN, 14));
			contentPanel.add(weightField);
		}
		{
			JLabel lblNewLabel_3 = new JLabel("Type");
			lblNewLabel_3.setFont(new Font("Tahoma", Font.PLAIN, 14));
			contentPanel.add(lblNewLabel_3);
		}
		{
			spType = new JSpinner();
			spType.setFont(new Font("Tahoma", Font.PLAIN, 14));
			spType.setModel(new SpinnerListModel(new String[] {"Bike", "EBike", "Twin Bike"}));
			contentPanel.add(spType);
		}
		{
			JLabel lblNewLabel_5 = new JLabel("Date");
			lblNewLabel_5.setFont(new Font("Tahoma", Font.PLAIN, 14));
			contentPanel.add(lblNewLabel_5);
		}
		{
			spDate = new JSpinner();
			spDate.setFont(new Font("Tahoma", Font.PLAIN, 14));
			spDate.setModel(new SpinnerDateModel(new Date(1608397200000L), null, null, Calendar.YEAR));
			contentPanel.add(spDate);
		}
		{
			JLabel lblNewLabel_4 = new JLabel("Station");
			lblNewLabel_4.setFont(new Font("Tahoma", Font.PLAIN, 14));
			contentPanel.add(lblNewLabel_4);
		}
		{
			stationField = new JTextField();
			stationField.setFont(new Font("Tahoma", Font.PLAIN, 14));
			contentPanel.add(stationField);
			stationField.setColumns(10);
		}
		{
			JLabel lblNewLabel_7 = new JLabel("Cost");
			lblNewLabel_7.setFont(new Font("Tahoma", Font.PLAIN, 14));
			contentPanel.add(lblNewLabel_7);
		}
		{
			costField = new JFormattedTextField(formatter);
			costField.setFont(new Font("Tahoma", Font.PLAIN, 14));
			contentPanel.add(costField);
		}
		{
			JLabel lblNewLabel_6 = new JLabel("Licence Plate");
			lblNewLabel_6.setFont(new Font("Tahoma", Font.PLAIN, 14));
			contentPanel.add(lblNewLabel_6);
		}
		{
			licencePlateField = new JTextField();
			licencePlateField.setFont(new Font("Tahoma", Font.PLAIN, 14));
			contentPanel.add(licencePlateField);
			licencePlateField.setColumns(10);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
				okButton.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						AddBikePopUp.this.onBtnOkClick();
						
					}
				});
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				cancelButton.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						AddBikePopUp.this.setVisible(false);
					}
					
				});
				buttonPane.add(cancelButton);
			}
		}
		{
			lbError = new JLabel("");
			lbError.setForeground(new Color(255, 51, 51));
			lbError.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lbError.setHorizontalAlignment(SwingConstants.CENTER);
			getContentPane().add(lbError, BorderLayout.CENTER);
		}
	}

	public void setController(AdminManageBikeController controller) {
		this.controller = controller;
	}
	
	private void onBtnOkClick() {
		HashMap<String, String> input= null;
		try {
			input = this.getInput();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (!(input == null) && this.validateInput(input)) {
			if (this.controller.addBike(input)) {
				this.setVisible(false);
				System.out.println("Add bike success!");
			}
			else {
				this.showError("Server reponse false");
			}
		}
		else {
			this.showError("Cần nhập đủ thông tin!");
		}
		
	}
	
	private boolean validateInput(HashMap<String,String> input) {
		
		for (String key : input.keySet()) {
			if (input.get(key).equals("")) {
				return false;
			}
		}
		return true;
	}
	
	private void showError(String error) {
		this.lbError.setText(error);
		int delay = 5000; //milliseconds
		   ActionListener taskPerformer = new ActionListener() {
		       public void actionPerformed(ActionEvent evt) {
		           AddBikePopUp.this.lbError.setText("");
		       }
		   };
		   new javax.swing.Timer(delay, taskPerformer).start();
	}

}
