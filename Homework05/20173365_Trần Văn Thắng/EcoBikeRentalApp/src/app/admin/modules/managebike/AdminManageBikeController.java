package app.admin.modules.managebike;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JPanel;

import abstractdata.controller.ADataPageController;
import abstractdata.controller.IDataManageController;
import abstractdata.gui.ADataListPane;
import abstractdata.gui.ADataSearchPane;
import abstractdata.gui.ADataSinglePane;
import api.ServerAPI;
import bean.*;

public class AdminManageBikeController extends ADataPageController<Vehicle> implements IDataManageController<Vehicle> {

	private AddBikePopUp addBikePopUp = new AddBikePopUp();
	@Override
	public ADataListPane<Vehicle> createListPane() {
		return new AdminBikeListPane(this);
	}

	@Override
	public ADataSearchPane createSearchPane() {
		return new AdminBikeSearchPane();
	}

	@Override
	public ADataSinglePane<Vehicle> createSinglePane() {
		return new BikeSinglePane();
	}

	@Override
	public List<? extends Vehicle> search(Map<String, String> searchParams) {
		return new ServerAPI().getVehicle(searchParams);
	}

	@Override
	public JPanel getDataPagePane() {
		this.pagePane = new AdminManageBikePage(super.searchPane, super.listPane);
		AdminManageBikePage adminPage = (AdminManageBikePage) this.pagePane;
		adminPage.setController(this);
		this.addBikePopUp.setController(this);
		return this.pagePane;
	}

	public void onBtnAddBikeClick() {
		// TODO ...
		this.addBikePopUp.setVisible(true);
	}
	
	@SuppressWarnings("deprecation")
	public boolean addBike(HashMap<String,String> info) {
		Vehicle newVehicle;
		switch(info.get("type")){
			case "bike":
				newVehicle = new Bike();
				break;
			case "ebike":
				newVehicle = new EBike();
				break;
			default :
				newVehicle = new TwinBike();
				break;
		}
		
		newVehicle.setId(info.get("id"));
		newVehicle.setName(info.get("name"));
		newVehicle.setCost(Integer.parseInt(info.get("cost")));
		newVehicle.setLicencePlate(info.get("licencePlate"));
		newVehicle.setProducer(info.get("produrcer"));
		newVehicle.setStationId(info.get("stationId"));
		newVehicle.setWeight(Float.parseFloat(info.get("weight")));
		newVehicle.setManufacturingDate(new Date());
		
		boolean addState = new ServerAPI().addVehicle(newVehicle);
		
		return addState;
	}
	
	@Override
	public boolean create(Vehicle t) {
		// TODO Auto-generated method stub
		System.out.println("Create Bike");
		return new ServerAPI().addVehicle(t);
	}

	@Override
	public void delete(Vehicle t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void read(Vehicle t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(Vehicle t) {
		// TODO Auto-generated method stub
		
	}
	
	
}
