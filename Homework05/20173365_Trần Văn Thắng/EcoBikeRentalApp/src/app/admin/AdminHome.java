package app.admin;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import abstractdata.controller.IDataSearchController;
import app.admin.modules.managebike.AdminBikeListPane;
import app.admin.modules.managebike.AdminBikeSearchPane;
import app.admin.modules.managebike.AdminManageBikeController;
import app.admin.modules.managebike.AdminManageBikePage;

import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

public class AdminHome extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdminController.getInstance().displayAdminHome();;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AdminHome() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblTitle = new JLabel("Admin Home");
		lblTitle.setFont(new Font("Tahoma", Font.PLAIN, 21));
		lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitle.setBounds(254, 10, 150, 50);
		contentPane.add(lblTitle);
		
		JButton btnManageBike = new JButton("Quản lý xe");
		btnManageBike.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnManageBike.setBounds(368, 129, 200, 50);
		contentPane.add(btnManageBike);
		btnManageBike.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				AdminController.getInstance().manageBike();
				
			}
		});
		
		JButton btnManageStation = new JButton("Quản lý bãi xe");
		btnManageStation.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnManageStation.setBounds(96, 129, 200, 50);
		contentPane.add(btnManageStation);
		btnManageStation.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				AdminController.getInstance().manageStation();
				
			}
		});
	}
}
