package api;
import java.util.ArrayList;
import java.util.Map;

import bean.Vehicle;
public interface IServerAPI {
	public ArrayList<Vehicle> getAllVehicle();
	public ArrayList<Vehicle> getVehicle(Map<String,String> queryParams);
	public boolean addVehicle(Vehicle v);
}
