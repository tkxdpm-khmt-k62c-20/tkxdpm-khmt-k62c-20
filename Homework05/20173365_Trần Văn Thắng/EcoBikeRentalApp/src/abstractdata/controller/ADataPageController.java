package abstractdata.controller;

import java.util.List;
import java.util.Map;

import javax.swing.JPanel;

import abstractdata.gui.ADataListPane;
import abstractdata.gui.ADataPagePane;
import abstractdata.gui.ADataSearchPane;
import abstractdata.gui.ADataSinglePane;

public abstract class ADataPageController<T> {
	protected ADataPagePane<T> pagePane;
	protected ADataSearchPane searchPane;
	protected ADataListPane<T> listPane;
	public ADataPageController() {
		this.searchPane = createSearchPane();

		this.listPane = createListPane();

		searchPane.setController(new IDataSearchController() {
			@Override
			public void search(Map<String, String> searchParams) {
				List<? extends T> list = ADataPageController.this.search(searchParams);
				listPane.updateData(list);
			}
		});

		searchPane.fireSearchEvent();

		//pagePane = new ADataPagePane<T>(searchPane, listPane);
	}

	public abstract ADataListPane<T> createListPane();

	public abstract ADataSearchPane createSearchPane();

	public abstract ADataSinglePane<T> createSinglePane();

	public JPanel getDataPagePane() {
		return pagePane;
	}

	public abstract List<? extends T> search(Map<String, String> searchParams);
}
