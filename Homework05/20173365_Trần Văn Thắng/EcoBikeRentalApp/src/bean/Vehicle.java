package bean;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
// @JsonTypeName("vehicle")
@JsonSubTypes({ @Type(value = Bike.class, name = "bike"), @Type(value = EBike.class, name = "ebike"),
		@Type(value = TwinBike.class, name = "twinbike"), })
public class Vehicle extends Bean {

	private String stationId;
	private String name;
	private float weight;
	private String licencePlate;
	private Date manufacturingDate;
	private String producer;
	private int cost;

	public Vehicle() {
	}

	public Vehicle(String name, String producer) {
		this.setName(name);
		this.setProducer(producer);
	}

	public Vehicle(String id, String stationId, String name, float weight, String licencePlate, Date manufacturingDate,
			String producer, int cost) {
		this.setId(id);
		this.setStationId(stationId);
		this.setName(name);
		this.setWeight(weight);
		this.setLicencePlate(licencePlate);
		this.setManufacturingDate(manufacturingDate);
		this.setProducer(producer);
		this.setCost(cost);
	}

	public int getCost() {
		return cost;
	}

	public String getLicencePlate() {
		return licencePlate;
	}

	public Date getManufacturingDate() {
		return manufacturingDate;
	}

	public String getName() {
		return this.name;
	}

	public String getProducer() {
		return producer;
	}

	public String getStationId() {
		return this.stationId;
	}

	public float getWeight() {
		return weight;
	}

	public boolean match(Vehicle vehicle) {
		if (!super.match(vehicle)) {
			return false;
		}

		if (vehicle.getStationId() != null && !this.getStationId().contains(vehicle.getStationId())) {
			return false;
		}

		if (vehicle.getName() != null && !this.getName().contains(vehicle.getName())) {
			return false;
		}

		if (vehicle.getWeight() != 0.0f && this.getWeight() != vehicle.getWeight()) {
			return false;
		}

		if (vehicle.getLicencePlate() != null && !this.getLicencePlate().contains(vehicle.getLicencePlate())) {
			return false;
		}

		if (vehicle.getManufacturingDate() != null
				&& this.getManufacturingDate().equals(vehicle.getManufacturingDate())) {
			return false;
		}

		if (vehicle.getProducer() != null && !this.getProducer().contains(vehicle.getProducer())) {
			return false;
		}

		if (vehicle.getCost() != 0 && this.getCost() != vehicle.getCost()) {
			return false;
		}

		return true;
	}

	public boolean matchStation(Station station) {
		return this.getStationId() == station.getId();
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public void setLicencePlate(String licencePlate) {
		this.licencePlate = licencePlate;
	}

	public void setManufacturingDate(Date manufacturingDate) {
		this.manufacturingDate = manufacturingDate;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setProducer(String producer) {
		this.producer = producer;
	}

	public void setStationId(String stationId) {
		this.stationId = stationId;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}

	@Override
	public String toString() {
		return this.id;
	}
}
