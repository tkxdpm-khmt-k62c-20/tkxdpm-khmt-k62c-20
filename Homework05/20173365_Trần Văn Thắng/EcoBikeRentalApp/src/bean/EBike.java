package bean;

import java.util.Date;

public class EBike extends Vehicle {

	private float batteryPercentage;
	private int loadCycles;

	public EBike() {
		super();
	}

	public EBike(String id, String stationId, String name, float weight, String licencePlate, Date manufacturingDate,
			String producer, int cost) {
		super(id, stationId, name, weight, licencePlate, manufacturingDate, producer, cost);
	}

	public EBike(String id, String stationId, String name, float weight, String licencePlate, Date manufacturingDate,
			String producer, int cost, float batteryPercentage, int loadCycles) {
		super(id, stationId, name, weight, licencePlate, manufacturingDate, producer, cost);
		this.setBatteryPercentage(batteryPercentage);
		this.setLoadCycles(loadCycles);
	}

	public float getBatteryPercentage() {
		return batteryPercentage;
	}

	public int getLoadCycles() {
		return loadCycles;
	}

	public void setBatteryPercentage(float batteryPercentage) {
		this.batteryPercentage = batteryPercentage;
	}

	public void setLoadCycles(int loadCycles) {
		this.loadCycles = loadCycles;
	}
}
