package bean;

public class Station extends Bean {

	private String name;
	private String address;
	private int numberOfBikes;
	private int numberOfEBikes;
	private int numberOfTwinBikes;
	private int numberOfEmptyDocks;
	private float distanceToCustomer;

	public Station() {
	}

	public Station(String name, String address) {
		this.setName(name);
		this.setAddress(address);
	}

	public Station(String id, String name, String address, int numberOfBikes, int numberOfEBikes, int numberOfTwinBikes,
			int numberOfEmptyDocks, int distanceToCustomer) {
		this.id = id;
		this.name = name;
		this.address = address;
		this.numberOfBikes = numberOfBikes;
		this.numberOfEBikes = numberOfEBikes;
		this.numberOfTwinBikes = numberOfTwinBikes;
		this.numberOfEmptyDocks = numberOfEmptyDocks;
		this.distanceToCustomer = distanceToCustomer;
	}

	public String getAddress() {
		return this.address;
	}

	public float getDistanceToCustomer() {
		return this.distanceToCustomer;
	}

	public String getName() {
		return this.name;
	}

	public int getNumberOfBikes() {
		return this.numberOfBikes;
	}

	public int getNumberOfEBikes() {
		return this.numberOfEBikes;
	}

	public int getNumberOfEmptyDocks() {
		return numberOfEmptyDocks;
	}

	public int getNumberOfTwinBikes() {
		return this.numberOfTwinBikes;
	}

	public boolean match(Station station) {
		if (!super.match(station)) {
			return false;
		}

		if (station.getName() != null && !this.getName().contains(station.getName())) {
			return false;
		}

		if (station.getAddress() != null && !this.getAddress().contains(station.getAddress())) {
			return false;
		}

		if (station.getNumberOfBikes() != 0 && this.getNumberOfBikes() != station.getNumberOfBikes()) {
			return false;
		}

		if (station.getNumberOfEBikes() != 0 && this.getNumberOfEBikes() != station.getNumberOfEBikes()) {
			return false;
		}

		if (station.getNumberOfTwinBikes() != 0 && this.getNumberOfTwinBikes() != station.getNumberOfTwinBikes()) {
			return false;
		}

		if (station.getNumberOfEmptyDocks() != 0 && this.getNumberOfEmptyDocks() != station.getNumberOfEmptyDocks()) {
			return false;
		}

		return true;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setDistanceToCustomer(float distanceToCustomer) {
		this.distanceToCustomer = distanceToCustomer;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setNumberOfBikes(int numberOfBikes) {
		this.numberOfBikes = numberOfBikes;
	}

	public void setNumberOfEBikes(int numberOfEBikes) {
		this.numberOfEBikes = numberOfEBikes;
	}

	public void setNumberOfEmptyDocks(int numberOfEmptyDocks) {
		this.numberOfEmptyDocks = numberOfEmptyDocks;
	}

	public void setNumberOfTwinBikes(int numberOfTwinBikes) {
		this.numberOfTwinBikes = numberOfTwinBikes;
	}

	@Override
	public String toString() {
		return this.getName();
	}
}
