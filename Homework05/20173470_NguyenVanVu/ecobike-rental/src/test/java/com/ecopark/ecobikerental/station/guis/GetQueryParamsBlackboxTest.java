package com.ecopark.ecobikerental.station.guis;

import static org.junit.Assert.assertTrue;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.awt.Component;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JTextField;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class GetQueryParamsBlackboxTest {

    private StationSearchPane stationSearchPane;
    private Map<String, String> inputValues;
    private Map<String, String> expectedResult;

    @Before
    public void initialize() {
        this.stationSearchPane = new StationSearchPane();

        for (Component component : stationSearchPane.getComponents()) {
            if (component instanceof JTextField) {
                JTextField textField = (JTextField) component;
                String componentName = component.getName();
                textField.setText(this.inputValues.get(componentName));
            }
        }
    }

    @Parameterized.Parameters
    public static Collection<Object> testCases() {
        ArrayList<Object> testCases = new ArrayList<>();
        String[] inputNames = { "hust", "", "hust" };
        String[] inputAddresses = { "hanoi", "hanoi", "" };

        ObjectMapper mapper = new ObjectMapper();
        ArrayList<HashMap<String, String>> expectedResults = new ArrayList<>();
        try {
            expectedResults.add(
                mapper.readValue(
                    "{\"name\": \"hust\", \"address\": \"hanoi\"}",
                    new TypeReference<HashMap<String, String>>() {}
                )
            );
            expectedResults.add(
                mapper.readValue(
                    "{\"address\": \"hanoi\"}",
                    new TypeReference<HashMap<String, String>>() {}
                )
            );
            expectedResults.add(
                mapper.readValue(
                    "{\"name\": \"hust\"}",
                    new TypeReference<HashMap<String, String>>() {}
                )
            );
        } catch (IOException e) {
            e.printStackTrace();
        }

        int numberOfTestCases = expectedResults.size();

        for (int i = 0; i < numberOfTestCases; ++i) {
            Map<String, String> inputValues = new HashMap<>();
            inputValues.put("name", inputNames[i]);
            inputValues.put("address", inputAddresses[i]);

            testCases.add(new Object[] { inputValues, expectedResults.get(i) });
        }

        return testCases;
    }

    public GetQueryParamsBlackboxTest(
        Map<String, String> inputValues,
        Map<String, String> expectedResult
    ) {
        this.inputValues = inputValues;
        this.expectedResult = expectedResult;
    }

    @Test
    public void testGetQueryParams() {
        assertTrue(
            expectedResult.equals(this.stationSearchPane.getQueryParams())
        );
    }
}
