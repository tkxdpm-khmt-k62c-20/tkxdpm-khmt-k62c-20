package com.ecopark.ecobikerental.bean;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import java.util.Date;

@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.PROPERTY,
    property = "type"
)
// @JsonTypeName("vehicle")
@JsonSubTypes(
    {
        @Type(value = Bike.class, name = "bike"),
        @Type(value = EBike.class, name = "ebike"),
        @Type(value = TwinBike.class, name = "twinbike"),
    }
)
public class Vehicle extends Bean {

    private String stationId;
    private String name;
    private float weight;
    private String licencePlate;
    private Date manufacturingDate;
    private String producer;
    private int cost;

    public Vehicle() {}

    public Vehicle(String name, String producer) {
        this.setName(name);
        this.setProducer(producer);
    }

    public Vehicle(
        String id,
        String stationId,
        String name,
        float weight,
        String licencePlate,
        Date manufacturingDate,
        String producer,
        int cost
    ) {
        this.setId(id);
        this.setStationId(stationId);
        this.setName(name);
        this.setWeight(weight);
        this.setLicencePlate(licencePlate);
        this.setManufacturingDate(manufacturingDate);
        this.setProducer(producer);
        this.setCost(cost);
    }

    public String getStationId() {
        return this.stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public String getLicencePlate() {
        return licencePlate;
    }

    public void setLicencePlate(String licencePlate) {
        this.licencePlate = licencePlate;
    }

    public Date getManufacturingDate() {
        return manufacturingDate;
    }

    public void setManufacturingDate(Date manufacturingDate) {
        this.manufacturingDate = manufacturingDate;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    @Override
    public String toString() {
        return this.id;
    }
}
