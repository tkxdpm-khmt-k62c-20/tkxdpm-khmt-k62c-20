package com.ecopark.ecobikerental.bean;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.PROPERTY,
    property = "type"
)
@JsonSubTypes(
    {
        @Type(value = Station.class, name = "station"),
        @Type(value = Vehicle.class, name = "vehicle"),
    }
)
public class Bean {

    protected String id;

    public Bean() {}

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
