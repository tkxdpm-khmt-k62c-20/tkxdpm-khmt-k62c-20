package com.ecopark.ecobikerental.user.controllers;

import com.ecopark.ecobikerental.factory.UserPageFactory;
import javax.swing.JPanel;

public class EBRUserController {

    public EBRUserController() {}

    public JPanel getPage(String pageID) {
        return UserPageFactory.instance().createPage(pageID);
    }
}
