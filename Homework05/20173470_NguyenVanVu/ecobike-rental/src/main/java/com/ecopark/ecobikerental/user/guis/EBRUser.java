package com.ecopark.ecobikerental.user.guis;

import com.ecopark.ecobikerental.user.controllers.EBRUserController;
import java.awt.*;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class EBRUser extends JFrame {

    public static final int WINDOW_WIDTH = 960;
    public static final int WINDOW_HEIGHT = 540;

    public EBRUser(EBRUserController controller) {
        // this.setLayout(layout);
        this.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel container = new JPanel();
        BorderLayout layout = new BorderLayout();
        container.setLayout(layout);
        container.setBorder(BorderFactory.createEmptyBorder(16, 16, 16, 16));

        JPanel headerPane = new HeaderPane();
        JPanel userStationPage = controller.getPage("station");
        container.add(headerPane, BorderLayout.NORTH);
        container.add(userStationPage, BorderLayout.CENTER);

        this.add(container);
        this.setVisible(true);
    }
}
