package com.ecopark.ecobikerental.api;

import com.ecopark.ecobikerental.bean.Station;
import java.util.ArrayList;
import java.util.Map;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class StationAPI implements IStationAPI {

    public static final String SERVER_HOST = "http://localhost:3000/";
    private static IStationAPI instance;
    private Client client;

    private StationAPI() {
        this.client = ClientBuilder.newClient();
    }

    @Override
    public ArrayList<Station> getStations(Map<String, String> queryParams) {
        Response response = createGETRequest(
            SERVER_HOST,
            "stations",
            queryParams
        );
        return readStationFromResponse(response);
    }

    private Response createGETRequest(
        String serverHost,
        String path,
        Map<String, String> queryParams
    ) {
        WebTarget webTarget = this.client.target(serverHost).path(path);

        if (queryParams != null) {
            for (String key : queryParams.keySet()) {
                String value = queryParams.get(key);
                webTarget = webTarget.queryParam(key, value);
            }
        }

        Invocation.Builder invocationBuilder = webTarget.request(
            MediaType.APPLICATION_JSON
        );

        return invocationBuilder.get();
    }

    private ArrayList<Station> readStationFromResponse(Response response) {
        ArrayList<Station> result = response.readEntity(
            new GenericType<ArrayList<Station>>() {}
        );
        return result;
    }

    public static IStationAPI instance() {
        if (instance == null) {
            synchronized (Station.class) {
                if (instance == null) {
                    instance = new StationAPI();
                }
            }
        }

        return instance;
    }
}
