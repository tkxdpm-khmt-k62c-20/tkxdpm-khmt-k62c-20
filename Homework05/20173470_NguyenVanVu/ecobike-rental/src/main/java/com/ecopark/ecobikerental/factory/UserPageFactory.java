package com.ecopark.ecobikerental.factory;

import com.ecopark.ecobikerental.abstractdata.guis.ADataPagePane;
import java.util.HashMap;

public class UserPageFactory {

    private static UserPageFactory instance;
    private HashMap<String, ADataPagePane<?>> registeredPage = new HashMap<>();

    private UserPageFactory() {}

    public static UserPageFactory instance() {
        if (instance == null) {
            synchronized (UserPageFactory.class) {
                if (instance == null) {
                    instance = new UserPageFactory();
                }
            }
        }

        return instance;
    }

    public void registerPage(String pageID, ADataPagePane<?> page) {
        this.registeredPage.put(pageID, page);
    }

    public ADataPagePane<?> createPage(String pageID) {
        return this.registeredPage.get(pageID).createPage();
    }
}
