package com.ecopark.ecobikerental.abstractdata.guis;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public abstract class ADataSinglePane<T>
    extends JPanel
    implements MouseListener {

    protected T t;
    protected GridBagLayout layout;
    protected GridBagConstraints c;
    protected Color backgroundColor;

    private JPanel panel;

    public ADataSinglePane() {
        this.backgroundColor = this.getBackground();
        this.setBorder(BorderFactory.createEmptyBorder(16, 16, 16, 16));
        this.addMouseListener(this);
        buildControls();
    }

    public ADataSinglePane(T t) {
        this();
        this.t = t;

        displayData();
    }

    public void buildControls() {
        layout = new GridBagLayout();
        this.setLayout(layout);
        c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 1;
    }

    public abstract void displayData();

    public void addDataHandlingComponent(Component component) {
        if (panel == null) {
            int row = getLastRowIndex();
            c.gridx = 0;
            c.gridy = row;
            panel = new JPanel();
            this.add(panel, c);
            panel.setLayout(new FlowLayout(FlowLayout.LEFT));
        }

        panel.add(component);
    }

    public void updateData(T t) {
        this.t = t;
        displayData();
    }

    public T getData() {
        return this.t;
    }

    protected int getLastRowIndex() {
        layout.layoutContainer(this);
        int[][] dim = layout.getLayoutDimensions();
        int rows = dim[1].length;
        return rows;
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        this.setCursor(new Cursor(Cursor.HAND_CURSOR));
    }

    @Override
    public void mouseExited(MouseEvent e) {
        this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }

    @Override
    public void mousePressed(MouseEvent e) {}

    @Override
    public void mouseReleased(MouseEvent e) {}
}
