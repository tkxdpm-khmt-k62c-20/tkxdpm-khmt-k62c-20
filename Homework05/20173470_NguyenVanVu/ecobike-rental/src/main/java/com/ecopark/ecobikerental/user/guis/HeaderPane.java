package com.ecopark.ecobikerental.user.guis;

import java.awt.*;
import javax.swing.JButton;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class HeaderPane extends JPanel {

    public HeaderPane() {
        FlowLayout layout = new FlowLayout(FlowLayout.RIGHT);
        this.setLayout(layout);

        JButton rentABikeButton = new JButton("Thuê xe");
        JButton rentingBikeButton = new JButton("Xe đang thuê");

        this.add(rentABikeButton);
        this.add(rentingBikeButton);
    }
}
