package com.ecopark.ecobikerental.station.guis;

import com.ecopark.ecobikerental.abstractdata.guis.ADataListPane;
import com.ecopark.ecobikerental.abstractdata.guis.ADataPagePane;
import com.ecopark.ecobikerental.abstractdata.guis.ADataSearchPane;
import com.ecopark.ecobikerental.bean.Station;
import com.ecopark.ecobikerental.factory.UserPageFactory;
import com.ecopark.ecobikerental.station.controllers.UserStationController;

@SuppressWarnings("serial")
public class UserStationPage extends ADataPagePane<Station> {
    static {
        UserPageFactory
            .instance()
            .registerPage("station", new UserStationPage());
    }

    public UserStationPage() {
        super();
    }

    public UserStationPage(
        ADataSearchPane searchPane,
        ADataListPane<Station> stationsListPane
    ) {
        super(searchPane, stationsListPane);
    }

    @Override
    public ADataPagePane<?> createPage() {
        UserStationController controller = new UserStationController();
        return (ADataPagePane<?>) controller.getDataPagePane();
    }
}
