package com.ecopark.ecobikerental.abstractdata.guis;

import javax.swing.JPanel;
import javax.swing.SpringLayout;

@SuppressWarnings("serial")
public class ADataPagePane<T> extends JPanel {

    private ADataSearchPane searchPane;
    private ADataListPane<T> listPane;

    public ADataPagePane() {}

    public ADataPagePane(
        ADataSearchPane searchPane,
        ADataListPane<T> listPane
    ) {
        this.searchPane = searchPane;
        this.listPane = listPane;

        SpringLayout layout = new SpringLayout();
        // BoxLayout layout = new BoxLayout(this, BoxLayout.Y_AXIS);
        this.setLayout(layout);

        this.add(this.searchPane);
        this.add(this.listPane);

        layout.putConstraint(
            SpringLayout.WEST,
            this.searchPane,
            5,
            SpringLayout.WEST,
            this
        );
        layout.putConstraint(
            SpringLayout.NORTH,
            this.searchPane,
            5,
            SpringLayout.NORTH,
            this
        );
        layout.putConstraint(
            SpringLayout.EAST,
            this.searchPane,
            -5,
            SpringLayout.EAST,
            this
        );

        layout.putConstraint(
            SpringLayout.WEST,
            this.listPane,
            5,
            SpringLayout.WEST,
            this
        );
        layout.putConstraint(
            SpringLayout.NORTH,
            this.listPane,
            5,
            SpringLayout.SOUTH,
            searchPane
        );
        layout.putConstraint(
            SpringLayout.EAST,
            this.listPane,
            -5,
            SpringLayout.EAST,
            this
        );
        layout.putConstraint(
            SpringLayout.SOUTH,
            this.listPane,
            -5,
            SpringLayout.SOUTH,
            this
        );
    }

    public ADataPagePane<?> createPage() {
        return new ADataPagePane<>();
    }
}
