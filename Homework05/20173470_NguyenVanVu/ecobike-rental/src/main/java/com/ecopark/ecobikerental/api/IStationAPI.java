package com.ecopark.ecobikerental.api;

import com.ecopark.ecobikerental.bean.Station;
import java.util.ArrayList;
import java.util.Map;

public interface IStationAPI {
    public ArrayList<Station> getStations(Map<String, String> queryParams);
}
