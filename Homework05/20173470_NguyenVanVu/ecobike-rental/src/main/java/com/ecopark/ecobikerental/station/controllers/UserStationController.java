package com.ecopark.ecobikerental.station.controllers;

import com.ecopark.ecobikerental.abstractdata.controllers.ADataPageController;
import com.ecopark.ecobikerental.abstractdata.guis.ADataListPane;
import com.ecopark.ecobikerental.abstractdata.guis.ADataSearchPane;
import com.ecopark.ecobikerental.abstractdata.guis.ADataSinglePane;
import com.ecopark.ecobikerental.api.IStationAPI;
import com.ecopark.ecobikerental.api.StationAPI;
import com.ecopark.ecobikerental.bean.Station;
import com.ecopark.ecobikerental.station.guis.StationSearchPane;
import com.ecopark.ecobikerental.station.guis.StationSinglePane;
import com.ecopark.ecobikerental.station.guis.UserStationsListPane;
import java.util.ArrayList;
import java.util.Map;

public class UserStationController extends ADataPageController<Station> {

    private ADataListPane<Station> listPane;

    public UserStationController() {
        super();
    }

    @Override
    public ADataSearchPane createSearchPane() {
        return new StationSearchPane();
    }

    @Override
    public ADataListPane<Station> createListPane() {
        this.listPane = new UserStationsListPane();
        return this.listPane;
    }

    @Override
    public ADataSinglePane<Station> createSinglePane() {
        return new StationSinglePane();
    }

    @Override
    public void search(Map<String, String> searchParams) {
        IStationAPI stationAPI = StationAPI.instance();
        ArrayList<Station> stations = stationAPI.getStations(searchParams);
        this.listPane.updateData(stations);
    }
}
