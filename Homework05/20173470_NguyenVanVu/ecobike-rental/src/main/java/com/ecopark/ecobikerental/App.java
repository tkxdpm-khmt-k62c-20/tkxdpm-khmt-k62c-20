package com.ecopark.ecobikerental;

import com.ecopark.ecobikerental.station.guis.UserStationPage;
import com.ecopark.ecobikerental.user.controllers.EBRUserController;
import com.ecopark.ecobikerental.user.guis.EBRUser;

public class App {

    public static void main(String[] args) {
        try {
            Class.forName(UserStationPage.class.getName().toString());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        EBRUserController userController = new EBRUserController();
        new EBRUser(userController);
    }
}
