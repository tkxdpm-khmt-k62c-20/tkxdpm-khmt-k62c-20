package com.ecopark.ecobikerental.bean;

import java.util.Date;

public class TwinBike extends Vehicle {

    public TwinBike() {
        super();
    }

    public TwinBike(
        String id,
        String stationId,
        String name,
        float weight,
        String licencePlate,
        Date manufacturingDate,
        String producer,
        int cost
    ) {
        super(
            id,
            stationId,
            name,
            weight,
            licencePlate,
            manufacturingDate,
            producer,
            cost
        );
    }
}
