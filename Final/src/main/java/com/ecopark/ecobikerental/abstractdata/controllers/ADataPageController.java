package com.ecopark.ecobikerental.abstractdata.controllers;

import java.util.Map;
import javax.swing.JPanel;

import com.ecopark.ecobikerental.abstractdata.guis.ADataListPane;
import com.ecopark.ecobikerental.abstractdata.guis.ADataPagePane;
import com.ecopark.ecobikerental.abstractdata.guis.ADataSearchPane;
import com.ecopark.ecobikerental.abstractdata.guis.ADataSinglePane;

public abstract class ADataPageController<T> {

    protected ADataPagePane<T> pagePane;
    protected ADataSearchPane searchPane;
    protected ADataListPane<T> listPane;

    public ADataPageController() {
        searchPane = this.createSearchPane();
        listPane = this.createListPane();

        searchPane.setController(
            new IDataSearchController() {
                @Override
                public void search(Map<String, String> searchParams) {
                    ADataPageController.this.search(searchParams);
                }
            }
        );
        listPane.setController(this);

        this.pagePane = new ADataPagePane<T>(searchPane, listPane);
        searchPane.fireSearchEvent();
    }

    public JPanel getDataPagePane() {
        return this.pagePane;
    }

    public abstract ADataSearchPane createSearchPane();

    public abstract ADataListPane<T> createListPane();

    public abstract ADataSinglePane<T> createSinglePane();

    public abstract void search(Map<String, String> searchParams);
}
