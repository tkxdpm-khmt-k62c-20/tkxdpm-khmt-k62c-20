package com.ecopark.ecobikerental.abstractdata.controllers;

public interface IDataManageController<T> {
    public boolean create(T t);

    public void read(T t);

    public void delete(T t);

    public void update(T t);
}
