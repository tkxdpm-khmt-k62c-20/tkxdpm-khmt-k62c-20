package com.ecopark.ecobikerental.abstractdata.controllers;

import java.util.Map;

public interface IDataSearchController {
    public void search(Map<String, String> searchParams);
}
