package com.ecopark.ecobikerental.bean;

import java.util.Date;

public class EBike extends Vehicle {

    private float batteryPercentage;
    private int loadCycles;

    public EBike() {
        super();
    }

    public EBike(
        String id,
        String stationId,
        String name,
        float weight,
        String licencePlate,
        Date manufacturingDate,
        String producer,
        int cost,
        boolean status
    ) {
        super(
            id,
            stationId,
            name,
            weight,
            licencePlate,
            manufacturingDate,
            producer,
            cost,
            status
        );
    }

    public EBike(
        String id,
        String stationId,
        String name,
        float weight,
        String licencePlate,
        Date manufacturingDate,
        String producer,
        int cost,
        float batteryPercentage,
        int loadCycles,
        boolean status
    ) {
        super(
            id,
            stationId,
            name,
            weight,
            licencePlate,
            manufacturingDate,
            producer,
            cost,
            status
        );
        this.setBatteryPercentage(batteryPercentage);
        this.setLoadCycles(loadCycles);
    }

    public float getBatteryPercentage() {
        return batteryPercentage;
    }

    public void setBatteryPercentage(float batteryPercentage) {
        this.batteryPercentage = batteryPercentage;
    }

    public int getLoadCycles() {
        return loadCycles;
    }

    public void setLoadCycles(int loadCycles) {
        this.loadCycles = loadCycles;
    }
}
