package com.ecopark.ecobikerental.bean;

public class Station extends Bean {

    private String name;
    private String address;
    private int numberOfBikes;
    private int numberOfEBikes;
    private int numberOfTwinBikes;
    private int numberOfEmptyDocks;
    private float distanceToCustomer;

    public Station() {}

    public Station(String name, String address) {
        this.setName(name);
        this.setAddress(address);
    }

    public Station(
        String id,
        String name,
        String address,
        int numberOfBikes,
        int numberOfEBikes,
        int numberOfTwinBikes,
        int numberOfEmptyDocks,
        int distanceToCustomer
    ) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.numberOfBikes = numberOfBikes;
        this.numberOfEBikes = numberOfEBikes;
        this.numberOfTwinBikes = numberOfTwinBikes;
        this.numberOfEmptyDocks = numberOfEmptyDocks;
        this.distanceToCustomer = distanceToCustomer;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getNumberOfBikes() {
        return this.numberOfBikes;
    }

    public void setNumberOfBikes(int numberOfBikes) {
        this.numberOfBikes = numberOfBikes;
    }

    public int getNumberOfEBikes() {
        return this.numberOfEBikes;
    }

    public void setNumberOfEBikes(int numberOfEBikes) {
        this.numberOfEBikes = numberOfEBikes;
    }

    public int getNumberOfTwinBikes() {
        return this.numberOfTwinBikes;
    }

    public void setNumberOfTwinBikes(int numberOfTwinBikes) {
        this.numberOfTwinBikes = numberOfTwinBikes;
    }

    public int getNumberOfEmptyDocks() {
        return numberOfEmptyDocks;
    }

    public void setNumberOfEmptyDocks(int numberOfEmptyDocks) {
        this.numberOfEmptyDocks = numberOfEmptyDocks;
    }

    public float getDistanceToCustomer() {
        return this.distanceToCustomer;
    }

    public void setDistanceToCustomer(float distanceToCustomer) {
        this.distanceToCustomer = distanceToCustomer;
    }

    @Override
    public String toString() {
        return this.getName();
    }
}
