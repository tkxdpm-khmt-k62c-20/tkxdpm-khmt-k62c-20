package com.ecopark.ecobikerental.bean;

public class User {
	private static User instance = null;
	public synchronized static User getInstance() {
		if (instance == null) {
			instance = new User();
		}
		return instance;
	}
	private String name;
	private String id;
	private Integer bankAccountId;
	private Integer balance;
	
	private User() {
		name = "Noname";
		id = null;
		bankAccountId = null;
		balance = 0;
	}
	
	public Integer getBalance() {
		return balance;
	}
	public void setBalance(Integer balance) {
		this.balance = balance;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Integer getBankAccountId() {
		return bankAccountId;
	}
	public void setBankAccountId(Integer bankAccountId) {
		this.bankAccountId = bankAccountId;
	}
}
