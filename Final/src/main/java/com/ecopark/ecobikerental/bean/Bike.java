package com.ecopark.ecobikerental.bean;

import java.util.Date;

public class Bike extends Vehicle {

    public Bike() {
        super();
    }

    public Bike(String name, String producer) {
        super(name, producer);
    }

    public Bike(
        String id,
        String stationId,
        String name,
        float weight,
        String licencePlate,
        Date manufacturingDate,
        String producer,
        int cost,
        boolean status
    ) {
        super(
            id,
            stationId,
            name,
            weight,
            licencePlate,
            manufacturingDate,
            producer,
            cost,
            status
        );
    }
}
