package com.ecopark.ecobikerental.api;
import java.util.ArrayList;
import java.util.Map;

import com.ecopark.ecobikerental.bean.Vehicle;
public interface IServerAPI {
	public ArrayList<Vehicle> getAllVehicle();
	public ArrayList<Vehicle> getVehicle(Map<String,String> queryParams);
	public boolean addVehicle(Vehicle v);
	public boolean checkBankAccountId(String bankAccountId);
	public boolean checkRental(String userId,String VehicleId,String AccountNumber);
}
