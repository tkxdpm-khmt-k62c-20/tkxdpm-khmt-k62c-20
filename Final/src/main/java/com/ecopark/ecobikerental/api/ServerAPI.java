package com.ecopark.ecobikerental.api;

import java.util.ArrayList;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ecopark.ecobikerental.bean.Bike;
import com.ecopark.ecobikerental.bean.EBike;
import com.ecopark.ecobikerental.bean.Vehicle;


public class ServerAPI implements IServerAPI {
	public static final String PATH = "http://localhost:3000/";
	
	private Client client;
	
	public ServerAPI() {
		client = ClientBuilder.newClient();
	}
	
	public ArrayList<Vehicle> getAllVehicle() {
		WebTarget webTarget = client.target(PATH).path("vehicles");

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		ArrayList<Vehicle> res = null;
		try {
			Response response = invocationBuilder.get();
			res = response.readEntity(new GenericType<ArrayList<Vehicle>>(){});
//			System.out.println(res);
//			System.out.println("first time");
		} catch(Exception e) {
			System.out.println("Cant connect to server");
			res = new ArrayList<Vehicle>();
		}
		return res;
	}

	public ArrayList<Vehicle> getVehicle(Map<String, String> queryParams) {
		WebTarget webTarget = client.target(PATH).path("vehicles");
		
		
		if (queryParams != null) {
			for (String key : queryParams.keySet()) {
				String value = queryParams.get(key);
				webTarget = webTarget.queryParam(key, value);
			}
		}
		

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		ArrayList<Vehicle> res = response.readEntity(new GenericType<ArrayList<Vehicle>>() {});
//		System.out.println(res);
		return res;
	}
	
	public ArrayList<Vehicle> getAllActiveBike(Map<String, String> queryParams) {
		WebTarget webTarget = client.target(PATH).path("vehicles/active");
		if (queryParams != null) {
			for (String key : queryParams.keySet()) {
				String value = queryParams.get(key);
				webTarget = webTarget.queryParam(key, value);
			}
		}
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		ArrayList<Vehicle> res = null;
		try {
			Response response = invocationBuilder.get();
			res = response.readEntity(new GenericType<ArrayList<Vehicle>>(){});
			System.out.println(res);
		} catch(Exception e) {
			System.out.println("Cant connect to server");
			res = new ArrayList<Vehicle>();
		}
		return res;
	}
	
	@Override
	public boolean addVehicle(Vehicle v) {
		WebTarget webTarget = client.target(PATH).path("vehicles");
		
		webTarget.queryParam("id", v.getId());
		webTarget.queryParam("stationId", v.getStationId());
		webTarget.queryParam("name", v.getName());
		webTarget.queryParam("weight", v.getWeight());
		webTarget.queryParam("licencePlate", v.getLicencePlate());
		webTarget.queryParam("manufacturingDate", v.getManufacturingDate());
		webTarget.queryParam("producer", v.getProducer());
		webTarget.queryParam("cost", v.getCost());
		
		System.out.println("add vehicle");
		System.out.println(String.format("Id : %s", v.getId()));
		System.out.println(String.format("stationId : %s", v.getStationId()));
		System.out.println(String.format("name : %s", v.getName()));
		System.out.println(String.format("weight : %f", v.getWeight()));
		System.out.println(String.format("licenePlate : %s", v.getLicencePlate()));
		System.out.println(String.format("date : %s", v.getManufacturingDate()));
		System.out.println(String.format("produceer : %s", v.getProducer()));
		System.out.println(String.format("cost : %d", v.getCost()));
		
		if (v instanceof Bike) {
			webTarget.queryParam("type", "bike");
		}
		else if (v instanceof EBike) {
			webTarget.queryParam("type", "ebike");
		}
		else {
			webTarget.queryParam("type", "twinbike");
		}

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(v, MediaType.APPLICATION_JSON));
		
		return response.getStatus()==201?true:false;
		// TODO add bike 
		
	}

	@Override
	public boolean checkBankAccountId(String bankAccountId) {
		// TODO Auto-generated method stub
		WebTarget webTarget = client.target(PATH).path("check-bank-account");
		webTarget = webTarget.queryParam("accountNumber", bankAccountId);

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		
		if (response.getStatus()==200) return true;
		
		return false;
	}

	@Override
	public boolean checkRental(String userId, String VehicleId, String accountNumber) {
		// TODO Auto-generated method stub
		WebTarget webTarget=client.target(PATH).path("vehicles/rent");
		userId = "user01";
		VehicleId = "bike01";
		accountNumber = "111222333444";
		webTarget = webTarget.queryParam("userId",userId);
		webTarget = webTarget.queryParam("vehicleId",VehicleId);
		webTarget = webTarget.queryParam("accountNumber",accountNumber);
		
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		System.out.println(response.readEntity(String.class));
		//if() return true;
		
		return true;
	}
	
	
	

}
