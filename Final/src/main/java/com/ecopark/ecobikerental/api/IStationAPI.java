package com.ecopark.ecobikerental.api;

import java.util.ArrayList;
import java.util.Map;

import com.ecopark.ecobikerental.bean.Station;

public interface IStationAPI {
    public ArrayList<Station> getStations(Map<String, String> queryParams);
    public boolean addStation(Station s);
}
