package com.ecopark.ecobikerental.api;

import java.util.ArrayList;
import java.util.Map;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ecopark.ecobikerental.bean.Station;

public class StationAPI implements IStationAPI {

    public static final String SERVER_HOST = "http://localhost:3000/";
    private Client client;

    public StationAPI() {
        this.client = ClientBuilder.newClient();
    }

    @Override
    public ArrayList<Station> getStations(Map<String, String> queryParams) {
        Response response = createGETRequest(
            SERVER_HOST,
            "stations",
            queryParams
        );
        return readStationFromResponse(response);
    }

    private Response createGETRequest(
        String serverHost,
        String path,
        Map<String, String> queryParams
    ) {
        WebTarget webTarget = this.client.target(serverHost).path(path);

        if (queryParams != null) {
            for (String key : queryParams.keySet()) {
                String value = queryParams.get(key);
                webTarget = webTarget.queryParam(key, value);
            }
        }

        Invocation.Builder invocationBuilder = webTarget.request(
            MediaType.APPLICATION_JSON
        );

        return invocationBuilder.get();
    }

    private ArrayList<Station> readStationFromResponse(Response response) {
        ArrayList<Station> result = response.readEntity(
            new GenericType<ArrayList<Station>>() {}
        );
        return result;
    }
    
    @Override
    public boolean addStation(Station s) {
		WebTarget webTarget = client.target(SERVER_HOST).path("stations");
		
		webTarget = webTarget.queryParam("id", s.getId());
//		webTarget.queryParam("type", s.getType());
		webTarget = webTarget.queryParam("name", s.getName());
		webTarget = webTarget.queryParam("address", s.getAddress());
		webTarget = webTarget.queryParam("numberOfBikes", s.getNumberOfBikes());
		webTarget = webTarget.queryParam("numberOfEBikes", s.getNumberOfEBikes());
		webTarget = webTarget.queryParam("numberOfTwinBikes", s.getNumberOfTwinBikes());
		webTarget = webTarget.queryParam("numberOfEmptyDocks", s.getNumberOfEmptyDocks());
		webTarget = webTarget.queryParam("distanceToCustomer", s.getDistanceToCustomer());
		
//		System.out.println("add station");
//		System.out.println(String.format("Id : %s", s.getId()));
//		System.out.println(String.format("name : %s", s.getName()));
//		System.out.println(String.format("address : %s", s.getAddress()));
//		System.out.println(String.format("numberOfBikes : %d", s.getNumberOfBikes()));
//		System.out.println(String.format("numberOfEBikes : %d", s.getNumberOfEBikes()));
//		System.out.println(String.format("numberOfTwinBikes : %d", s.getNumberOfTwinBikes()));
//		System.out.println(String.format("numberOfEmptyDocks : %d", s.getNumberOfEmptyDocks()));
//		System.out.println(String.format("distanceToCustomer : %f", s.getDistanceToCustomer()));
		
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(s, MediaType.APPLICATION_JSON));
		return response.getStatus()==201?true:false;
		// TODO add station
		
	}
}
