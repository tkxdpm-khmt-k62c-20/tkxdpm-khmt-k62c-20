package com.ecopark.ecobikerental.mycomponents;

import java.awt.Dimension;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Gap extends JPanel {

    public Gap(int width, int height) {
        this.setPreferredSize(new Dimension(width, height));
    }
}
