package com.ecopark.ecobikerental.mycomponents;

import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.apache.batik.swing.JSVGCanvas;

@SuppressWarnings("serial")
public class LabelWithIcon extends JPanel {

    public LabelWithIcon(JLabel label, JSVGCanvas svg) {
        this.setLayout(new FlowLayout(FlowLayout.LEFT));
        this.add(svg);
        this.add(label);
    }
}
