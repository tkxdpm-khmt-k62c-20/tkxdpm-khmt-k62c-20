package com.ecopark.ecobikerental.user.payment.guis;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.ecopark.ecobikerental.user.payment.controller.PaymentController;

public class PaymentConfirm extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JButton btn_Confirm;
	private JButton btn_Back;
	
	private PaymentController controller;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PaymentConfirm frame = new PaymentConfirm();
					frame.setVisible(true);
					frame.extracted();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PaymentConfirm() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel_Confirm = new JPanel();
		contentPane.add(panel_Confirm, BorderLayout.SOUTH);
		
		btn_Confirm = new JButton("X\u00E1c nh\u1EADn");
		panel_Confirm.add(btn_Confirm);
		
		JPanel panel_Title = new JPanel();
		contentPane.add(panel_Title, BorderLayout.NORTH);
		GridBagLayout gbl_panel_Title = new GridBagLayout();
		gbl_panel_Title.columnWidths = new int[]{115, 89, 100, 0};
		gbl_panel_Title.rowHeights = new int[]{23, 0};
		gbl_panel_Title.columnWeights = new double[]{0.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_panel_Title.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		panel_Title.setLayout(gbl_panel_Title);
		
		btn_Back = new JButton("Back");
		GridBagConstraints gbc_btn_Back = new GridBagConstraints();
		gbc_btn_Back.anchor = GridBagConstraints.WEST;
		gbc_btn_Back.insets = new Insets(0, 0, 5, 5);
		gbc_btn_Back.gridx = 0;
		gbc_btn_Back.gridy = 1;
		panel_Title.add(btn_Back, gbc_btn_Back);
		
		JLabel lbl_Confirm = new JLabel("X\u00E1c nh\u1EADn thanh to\u00E1n");
		GridBagConstraints gbc_lbl_Confirm = new GridBagConstraints();
		gbc_lbl_Confirm.insets = new Insets(0, 0, 5, 0);
		gbc_lbl_Confirm.anchor = GridBagConstraints.WEST;
		gbc_lbl_Confirm.gridx = 2;
		gbc_lbl_Confirm.gridy = 0;
		panel_Title.add(lbl_Confirm, gbc_lbl_Confirm);
		
		JLabel lblNewLabel = new JLabel("S\u1ED1 t\u00E0i kho\u1EA3n");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 0, 5);
		gbc_lblNewLabel.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 4;
		panel_Title.add(lblNewLabel, gbc_lblNewLabel);
		
		textField = new JTextField();
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 1;
		gbc_textField.gridy = 4;
		gbc_textField.gridwidth=2;
		panel_Title.add(textField, gbc_textField);
		textField.setColumns(10);
		
		this.controller=new PaymentController();
		this.extracted();

	}

	private void extracted() {
		btn_Back.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				PaymentConfirm.this.setVisible(false);
			}
		});
		
		btn_Confirm.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				String text= textField.getText().trim();
				if(text.equals("")) {
					JFrame frame=new JFrame();
					frame.setBounds(200, 200, 300, 200);
					JLabel label=new JLabel("Cần nhập thông tin số tài khoản");
					frame.add(label);
					frame.setVisible(true);
				}			
				else {
					System.out.println("new PaymentConfirm");
					boolean check_Bank=PaymentConfirm.this.controller.searchAccountBank(textField.getText().trim());
					System.out.println("new PaymentConfirm");
					if(check_Bank) {
						System.out.println("response OK");
						Payment pay=new Payment();
						pay.setLabel(PaymentConfirm.this.textField.getText().trim());				
						pay.setVisible(true);
					}
				}
			}
			
		});
	}

}
