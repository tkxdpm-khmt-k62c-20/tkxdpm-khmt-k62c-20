package com.ecopark.ecobikerental.user.payment.controller;

import com.ecopark.ecobikerental.api.ServerAPI;

public class PaymentController {

	public PaymentController() {}
	
	public boolean searchAccountBank(String accountBank) {
		return new ServerAPI().checkBankAccountId(accountBank);
	}
}
