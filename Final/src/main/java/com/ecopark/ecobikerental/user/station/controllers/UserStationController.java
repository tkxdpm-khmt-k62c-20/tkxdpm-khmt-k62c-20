package com.ecopark.ecobikerental.user.station.controllers;

import com.ecopark.ecobikerental.abstractdata.controllers.ADataPageController;
import com.ecopark.ecobikerental.abstractdata.guis.ADataListPane;
import com.ecopark.ecobikerental.abstractdata.guis.ADataSearchPane;
import com.ecopark.ecobikerental.abstractdata.guis.ADataSinglePane;
import com.ecopark.ecobikerental.api.StationAPI;
import com.ecopark.ecobikerental.bean.Station;
import com.ecopark.ecobikerental.user.station.guis.StationSearchPane;
import com.ecopark.ecobikerental.user.station.guis.StationSinglePane;
import com.ecopark.ecobikerental.user.station.guis.UserStationsListPane;

import java.util.ArrayList;
import java.util.Map;

public class UserStationController extends ADataPageController<Station> {

    public UserStationController() {
        super();
    }

    @Override
    public ADataSearchPane createSearchPane() {
        return new StationSearchPane();
    }

    @Override
    public ADataListPane<Station> createListPane() {
        return new UserStationsListPane();
    }

    @Override
    public ADataSinglePane<Station> createSinglePane() {
        return new StationSinglePane();
    }

    @Override
    public void search(Map<String, String> searchParams) {
        ArrayList<Station> stations = new StationAPI()
        .getStations(searchParams);
        this.pagePane.getListPane().updateData(stations);
    }
}
