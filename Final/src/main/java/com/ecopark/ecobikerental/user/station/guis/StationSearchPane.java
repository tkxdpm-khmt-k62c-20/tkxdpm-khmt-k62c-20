package com.ecopark.ecobikerental.user.station.guis;

import java.util.Map;
import javax.swing.JLabel;
import javax.swing.JTextField;

import com.ecopark.ecobikerental.abstractdata.guis.ADataSearchPane;

@SuppressWarnings("serial")
public class StationSearchPane extends ADataSearchPane {

    private JTextField nameField;
    private JTextField addressField;

    public StationSearchPane() {
        super();
    }

    @Override
    public void buildControls() {
        JLabel nameLabel = new JLabel("Tên");
        JLabel addressLabel = new JLabel("Địa chỉ");
        nameField = new JTextField(20);
        nameField.setName("name");
        addressField = new JTextField(20);
        addressField.setName("address");

        int row = this.getLastRowIndex();
        this.c.gridx = 0;
        this.c.gridy = row;
        this.add(nameLabel, c);

        this.c.gridx = 1;
        this.c.gridy = row;
        this.add(nameField, c);

        row = this.getLastRowIndex();
        this.c.gridx = 0;
        this.c.gridy = row;
        this.add(addressLabel, c);

        this.c.gridx = 1;
        this.c.gridy = row;
        this.add(addressField, c);
    }

    @Override
    public Map<String, String> getQueryParams() {
        Map<String, String> result = super.getQueryParams();

        String name = this.nameField.getText().trim();
        String address = this.addressField.getText().trim();
        if (!name.equals("")) {
            result.put("name", name);
        }
        if (!address.equals("")) {
            result.put("address", address);
        }

        return result;
    }
}
