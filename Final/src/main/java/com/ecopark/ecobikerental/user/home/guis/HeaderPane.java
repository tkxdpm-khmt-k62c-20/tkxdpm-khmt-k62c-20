package com.ecopark.ecobikerental.user.home.guis;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import com.ecopark.ecobikerental.user.rental.guis.RentalPage;

@SuppressWarnings("serial")
public class HeaderPane extends JPanel {

    public HeaderPane() {
        FlowLayout layout = new FlowLayout(FlowLayout.RIGHT);
        this.setLayout(layout);

        JButton rentABikeButton = new JButton("Thuê xe");
        
        RentalPage rentalPage = new RentalPage();
        
        rentABikeButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				rentalPage.setVisible(true);
			}
		});
        JButton rentingBikeButton = new JButton("Xe đang thuê");

        this.add(rentABikeButton);
        this.add(rentingBikeButton);
    }
}
