package com.ecopark.ecobikerental.user.station.guis;

import com.ecopark.ecobikerental.abstractdata.guis.ADataSinglePane;
import com.ecopark.ecobikerental.bean.Station;
import com.ecopark.ecobikerental.mycomponents.Gap;
import com.ecopark.ecobikerental.mycomponents.LabelWithIcon;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.apache.batik.swing.JSVGCanvas;

@SuppressWarnings("serial")
public class StationSinglePane extends ADataSinglePane<Station> {

    private JLabel nameLabel;
    private JLabel addressLabel;
    private JLabel distanceToCustomerLabel;
    private JLabel numberOfBikesLabel;
    private JLabel numberOfEBikesLabel;
    private JLabel numberOfTwinBikesLabel;
    private JLabel numberOfEmptyDocksLabel;

    public StationSinglePane() {
        super();
    }

    public StationSinglePane(Station station) {
        super(station);
    }

    @Override
    public void buildControls() {
        super.buildControls();
        this.nameLabel = new JLabel();
        this.nameLabel.setFont(
                new Font(this.nameLabel.getFont().getName(), Font.BOLD, 18)
            );
        this.addressLabel = new JLabel();
        this.distanceToCustomerLabel = new JLabel();
        this.numberOfBikesLabel = new JLabel();
        this.numberOfEBikesLabel = new JLabel();
        this.numberOfTwinBikesLabel = new JLabel();
        this.numberOfEmptyDocksLabel = new JLabel();
        HashMap<String, JSVGCanvas> svgResources = this.loadSVGResources();

        int row = this.getLastRowIndex();
        c.gridx = 0;
        c.gridy = row;
        this.add(this.nameLabel, c);

        row = this.getLastRowIndex();
        c.gridx = 0;
        c.gridy = row;
        JPanel addressPanel = new JPanel();
        addressPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        addressPanel.add(
            new LabelWithIcon(
                this.distanceToCustomerLabel,
                svgResources.get("pin")
            )
        );
        addressPanel.add(new Gap(20, 0));
        addressPanel.add(this.addressLabel, c);
        this.add(addressPanel, c);

        row = this.getLastRowIndex();
        c.gridx = 0;
        c.gridy = row;
        JPanel availbleBikePanel = new JPanel();
        availbleBikePanel.setLayout(new GridLayout(1, 4));
        availbleBikePanel.add(
            new LabelWithIcon(this.numberOfBikesLabel, svgResources.get("bike"))
        );
        availbleBikePanel.add(
            new LabelWithIcon(
                this.numberOfEBikesLabel,
                svgResources.get("electric-bike")
            )
        );
        availbleBikePanel.add(
            new LabelWithIcon(
                this.numberOfTwinBikesLabel,
                svgResources.get("twin-bike")
            )
        );
        availbleBikePanel.add(this.numberOfEmptyDocksLabel);
        this.add(availbleBikePanel, c);
    }

    @Override
    public void displayData() {
        this.nameLabel.setText(this.t.getName());
        this.addressLabel.setText(this.t.getAddress());
        this.distanceToCustomerLabel.setText(
                Float.toString(this.t.getDistanceToCustomer())
            );
        this.numberOfBikesLabel.setText(
                Integer.toString(this.t.getNumberOfBikes())
            );
        this.numberOfEBikesLabel.setText(
                Integer.toString(this.t.getNumberOfEBikes())
            );
        this.numberOfTwinBikesLabel.setText(
                Integer.toString(this.t.getNumberOfTwinBikes())
            );
        this.numberOfEmptyDocksLabel.setText(
                String.format(
                    "Số docks xe trống: %d",
                    this.t.getNumberOfEmptyDocks()
                )
            );
    }

    @Override
    public void mouseClicked(MouseEvent e) {}

    private HashMap<String, JSVGCanvas> loadSVGResources() {
        HashMap<String, String> resourcePaths = new HashMap<String, String>();
        resourcePaths.put("pin", "/svg/room-24px.svg");
        resourcePaths.put("bike", "/svg/pedal_bike-24px.svg");
        resourcePaths.put("electric-bike", "/svg/electric_bike-24px.svg");
        resourcePaths.put("twin-bike", "/svg/tandem-24px.svg");
        // resourcePaths.put("pin", "/svg/pin.svg");
        HashMap<String, JSVGCanvas> svgResources = new HashMap<String, JSVGCanvas>();
        for (String key : resourcePaths.keySet()) {
            JSVGCanvas svg = new JSVGCanvas();
            String path = resourcePaths.get(key);
            svg.setURI(this.getClass().getResource(path).toString());
            svg.setBackground(this.backgroundColor);
            svg.setPreferredSize(new Dimension(24, 24));
            svg.setEnableZoomInteractor(false);
            svgResources.put(key, svg);
        }
        return svgResources;
    }
}
