package com.ecopark.ecobikerental.user.payment.guis;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.SwingConstants;

public class Payment extends JFrame {

	private JPanel contentPane;
	private JButton btn_Confirm;
	private JButton btn_Back;
	private JLabel lbl_display;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Payment frame = new Payment();
					frame.setVisible(true);
					frame.extracted();
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Payment() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel_Confirm = new JPanel();
		contentPane.add(panel_Confirm, BorderLayout.SOUTH);
		
		btn_Confirm = new JButton("Thanh to�n");
		panel_Confirm.add(btn_Confirm);
		
		JPanel panel_Title = new JPanel();
		contentPane.add(panel_Title, BorderLayout.NORTH);
		GridBagLayout gbl_panel_Title = new GridBagLayout();
		gbl_panel_Title.columnWidths = new int[]{115, 0, 89, 100, 0};
		gbl_panel_Title.rowHeights = new int[]{23, 0, 0, 0, 0, 0};
		gbl_panel_Title.columnWeights = new double[]{0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_panel_Title.rowWeights = new double[]{0.0, Double.MIN_VALUE, 0.0, 0.0, 0.0, 0.0};
		panel_Title.setLayout(gbl_panel_Title);
		
		btn_Back = new JButton("Back");
		GridBagConstraints gbc_btn_Back = new GridBagConstraints();
		gbc_btn_Back.anchor = GridBagConstraints.WEST;
		gbc_btn_Back.insets = new Insets(0, 0, 5, 5);
		gbc_btn_Back.gridx = 0;
		gbc_btn_Back.gridy = 1;
		panel_Title.add(btn_Back, gbc_btn_Back);
		
		JLabel lbl_Confirm = new JLabel("               Thanh to\u00E1n");
		GridBagConstraints gbc_lbl_Confirm = new GridBagConstraints();
		gbc_lbl_Confirm.insets = new Insets(0, 0, 5, 0);
		gbc_lbl_Confirm.anchor = GridBagConstraints.WEST;
		gbc_lbl_Confirm.gridx = 3;
		gbc_lbl_Confirm.gridy = 0;
		panel_Title.add(lbl_Confirm, gbc_lbl_Confirm);
		
		JLabel lbl_sotien = new JLabel("Số tiền phải trả");
		lbl_sotien.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc_lbl_sotien = new GridBagConstraints();
		gbc_lbl_sotien.insets = new Insets(0, 0, 5, 5);
		gbc_lbl_sotien.gridx = 0;
		gbc_lbl_sotien.gridy = 6;
		panel_Title.add(lbl_sotien, gbc_lbl_sotien);
		
		JLabel lbl_thanhtoan = new JLabel("115000");
		GridBagConstraints gbc_lbl_thanhtoan = new GridBagConstraints();
		gbc_lbl_thanhtoan.insets = new Insets(0, 0, 5, 5);
		gbc_lbl_thanhtoan.gridx = 1;
		gbc_lbl_thanhtoan.gridy = 6;
		panel_Title.add(lbl_thanhtoan, gbc_lbl_thanhtoan);
		
		JLabel lbl_stk = new JLabel("S\u1ED1 t\u00E0i kho\u1EA3n");
		lbl_stk.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lbl_stk = new GridBagConstraints();
		gbc_lbl_stk.insets = new Insets(0, 0, 0, 5);
		gbc_lbl_stk.anchor = GridBagConstraints.EAST;
		gbc_lbl_stk.gridx = 0;
		gbc_lbl_stk.gridy = 5;
		panel_Title.add(lbl_stk, gbc_lbl_stk);
		
		lbl_display = new JLabel("123456789");
		GridBagConstraints gbc_lbl_display = new GridBagConstraints();
		gbc_lbl_display.insets = new Insets(0, 0, 0, 5);
		gbc_lbl_display.gridx = 1;
		gbc_lbl_display.gridy = 5;
		panel_Title.add(lbl_display, gbc_lbl_display);
		
		extracted();
		
	}

	private void extracted() {
		btn_Back.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				Payment.this.setVisible(false);
			}
		});
		
        btn_Confirm.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				JFrame frame=new JFrame();
				frame.setBounds(200, 200, 200, 200);
				JLabel label=new JLabel("Thanh toán thành công");
				frame.add(label);
				frame.setVisible(true);
			}
		});
	}
	
	public void setLabel(String label) {
		this.lbl_display.setText(label);
	}

}
