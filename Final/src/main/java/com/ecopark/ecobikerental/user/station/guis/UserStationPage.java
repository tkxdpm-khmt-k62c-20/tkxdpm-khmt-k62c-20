package com.ecopark.ecobikerental.user.station.guis;

import com.ecopark.ecobikerental.abstractdata.guis.ADataListPane;
import com.ecopark.ecobikerental.abstractdata.guis.ADataPagePane;
import com.ecopark.ecobikerental.abstractdata.guis.ADataSearchPane;
import com.ecopark.ecobikerental.bean.Station;

@SuppressWarnings("serial")
public class UserStationPage extends ADataPagePane<Station> {

    public UserStationPage() {
        super();
    }

    public UserStationPage(
        ADataSearchPane searchPane,
        ADataListPane<Station> stationsListPane
    ) {
        super(searchPane, stationsListPane);
    }
}
