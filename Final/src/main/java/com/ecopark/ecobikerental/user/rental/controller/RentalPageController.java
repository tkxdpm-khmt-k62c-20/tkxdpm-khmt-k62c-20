package com.ecopark.ecobikerental.user.rental.controller;

import java.util.HashMap;
import java.util.List;

import com.ecopark.ecobikerental.api.ServerAPI;
import com.ecopark.ecobikerental.bean.Vehicle;

public class RentalPageController {
	
	public RentalPageController() {};
	
	public List<Vehicle> search(String id){
		HashMap<String,String> searchParams = new HashMap<String,String>();
		searchParams.put("vehicleId", id);
		return new ServerAPI().getVehicle(searchParams);
	}
	
	public boolean search_Rental(String userId,String vehicleId,String accountNumber)
	{
		return new ServerAPI().checkRental(userId,vehicleId,accountNumber);
	}

}
