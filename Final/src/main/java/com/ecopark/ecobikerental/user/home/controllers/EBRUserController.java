package com.ecopark.ecobikerental.user.home.controllers;

import javax.swing.JPanel;

import com.ecopark.ecobikerental.user.station.controllers.UserStationController;

public class EBRUserController {

    public EBRUserController() {}

    public JPanel getStationPage() {
        UserStationController userStationController = new UserStationController();
        return userStationController.getDataPagePane();
    }
}
