package com.ecopark.ecobikerental.user.station.guis;

import com.ecopark.ecobikerental.abstractdata.guis.ADataListPane;
import com.ecopark.ecobikerental.abstractdata.guis.ADataSinglePane;
import com.ecopark.ecobikerental.bean.Station;

@SuppressWarnings("serial")
public class UserStationsListPane extends ADataListPane<Station> {

    public UserStationsListPane() {
        super();
    }

    @Override
    public void decorateSinglePane(ADataSinglePane<Station> singlePane) {}
}
