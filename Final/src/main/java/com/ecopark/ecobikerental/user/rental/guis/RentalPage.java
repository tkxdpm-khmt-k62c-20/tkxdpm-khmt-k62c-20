package com.ecopark.ecobikerental.user.rental.guis;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.ecopark.ecobikerental.bean.User;
import com.ecopark.ecobikerental.bean.Vehicle;
import com.ecopark.ecobikerental.user.payment.guis.PaymentConfirm;
import com.ecopark.ecobikerental.user.rental.controller.RentalPageController;

public class RentalPage extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JButton btn_Back;
	private JButton btn_Search;
	private JButton btn_Rental;
	private JPanel panel;
	private JLabel lblId;
	private JLabel lblWeight;
	private JLabel lblStationId;
	private JLabel lblManufacturingDate;
	private JLabel lblName;
	private JLabel lblProducer;
	private JLabel lblLiencenPlate;
	private JLabel lblCost;
	
	private RentalPageController controller;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RentalPage frame = new RentalPage();
					frame.setVisible(true);
					frame.extracted();
					//frame.setVisible(false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public RentalPage() {
		prepareGUI();
		
		// debug
		this.controller = new RentalPageController();
	}
	
	private void prepareGUI() 
	{
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 400);
		getContentPane().setLayout(new GridLayout(4,1));
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(100,100));
		setContentPane(contentPane);
		
		JPanel panel_Rental = new JPanel();
		panel_Rental.setBorder(new EmptyBorder(0, 225, 0, 225));
		contentPane.add(panel_Rental, BorderLayout.SOUTH);
		panel_Rental.setLayout(new GridLayout(1, 0, 0, 0));
		
		btn_Rental = new JButton("Thu\u00EA xe");
		panel_Rental.add(btn_Rental);
		
		JPanel panel_Back = new JPanel();
		panel_Back.setBorder(new EmptyBorder(0, 0, 0, 0));
		contentPane.add(panel_Back, BorderLayout.NORTH);
		GridBagLayout gbl_panel_Back = new GridBagLayout();
		gbl_panel_Back.columnWeights = new double[]{1.0, 1.0, 0.0};
		panel_Back.setLayout(gbl_panel_Back);
		
		GridBagConstraints gbc_Back=new GridBagConstraints();
		gbc_Back.insets = new Insets(0, 0, 5, 5);
		gbc_Back.anchor = GridBagConstraints.WEST;
		gbc_Back.gridx=0;
		gbc_Back.gridy=1;
		
		btn_Back = new JButton("Back");
		panel_Back.add(btn_Back,gbc_Back);
		
		JLabel lbl_Title = new JLabel("RENTAL PAGE");
		lbl_Title.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lbl_Title = new GridBagConstraints();
		gbc_lbl_Title.insets = new Insets(0, 0, 5, 5);
		gbc_lbl_Title.gridx = 1;
		gbc_lbl_Title.gridy = 0;
		panel_Back.add(lbl_Title, gbc_lbl_Title);
		
		JLabel lbl_ID = new JLabel("ID");
		GridBagConstraints gbc_lbl_ID = new GridBagConstraints();
		gbc_lbl_ID.insets = new Insets(0, 0, 0, 5);
		gbc_lbl_ID.anchor = GridBagConstraints.EAST;
		gbc_lbl_ID.gridx = 0;
		gbc_lbl_ID.gridy = 2;
		panel_Back.add(lbl_ID, gbc_lbl_ID);
		
		textField = new JTextField();
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.insets = new Insets(0, 0, 0, 5);
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 1;
		gbc_textField.gridy = 2;
		panel_Back.add(textField, gbc_textField);
		textField.setColumns(35);
		
		GridBagConstraints gbc_btnSearch=new GridBagConstraints();
		gbc_btnSearch.gridx=2;
		gbc_btnSearch.gridy=2;
		
		btn_Search=new JButton("Search");
		panel_Back.add(btn_Search,gbc_btnSearch);	
		
		panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(0, 2, 0, 0));
		
		
		
		lblId = new JLabel("ID");
		lblId.setFont(new Font("Tahoma", Font.PLAIN, 14));
		panel.add(lblId);
		
		lblStationId = new JLabel("StationId");
		panel.add(lblStationId);
		
		lblName = new JLabel("Name");
		panel.add(lblName);
		
		lblWeight = new JLabel("Weight");
		panel.add(lblWeight);
		
		lblLiencenPlate = new JLabel("LiencePlate");
		panel.add(lblLiencenPlate);
		
		lblManufacturingDate = new JLabel("manufacturingDate");
		panel.add(lblManufacturingDate);
		
		lblProducer = new JLabel("Producer");
		panel.add(lblProducer);
		
		lblCost = new JLabel("Cost");
		panel.add(lblCost);
		
		extracted();

	}

	private void extracted() {
		
		JFrame frame=new JFrame();
		frame.setBounds(200, 200, 300, 200);
		
		btn_Back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				RentalPage.this.setVisible(false);
			}
			
		});
		
		btn_Rental.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String text=textField.getText().trim();
				if(text.equals(""))
				{
					JLabel label=new JLabel("Cần nhập thông tin");
					frame.add(label);
					frame.setVisible(true);
				}
				else {
					List<Vehicle> result = RentalPage.this.controller.search(RentalPage.this.textField.getText().trim());
					if(result.size()==0)
					{
						JLabel label=new JLabel("Không tìm thấy xe");
						frame.add(label);
						frame.setVisible(true);
					}
					
					else {
						User user=User.getInstance();
						user.setId("user01");
						user.setBankAccountId(123566);
						boolean check=RentalPage.this.controller.search_Rental(user.getId(),result.get(0).getId(), user.getBankAccountId().toString());
						if(check) {
							PaymentConfirm pay_cfm=new PaymentConfirm();
							pay_cfm.setVisible(true);
							
						} 
						else {
							JLabel lbl_msg=new JLabel("Khong the thue xe");
							frame.add(lbl_msg);
							frame.setVisible(true);
						}
					}
				}
			}
		});
		
		btn_Search.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
				String text=textField.getText().trim();
				if(text.equals(""))
				{
					JLabel label=new JLabel("Cần nhập thông tin");
					frame.add(label);
					frame.setVisible(true);
				}
				else {
					System.out.println("searching.... id :"+RentalPage.this.textField.getText().trim());
					List<Vehicle> result = RentalPage.this.controller.search(RentalPage.this.textField.getText().trim());
					System.out.println(String.format("so xe tim ddk :%d",result.size()));
					
					if(result.size()==0)
					{
						JLabel label=new JLabel("Không tìm thấy xe");
						frame.add(label);
						frame.setVisible(true);
					}
					else {
						String s="ID: "+result.get(0).getId();
						lblId.setText(s);
						s="StationID: "+result.get(0).getStationId();
						lblStationId.setText(s);
						s="Weight: "+ Float.toString(result.get(0).getWeight());
						lblWeight.setText(s);
						s="Name: "+result.get(0).getName();
						lblName.setText(s);
						s= "ManufactoringDate: "+(result.get(0).getManufacturingDate().toString());
						lblManufacturingDate.setText(s);
						s="Cost: "+Float.toString(result.get(0).getCost());
						lblCost.setText(s);
						s="LicencePlate: "+result.get(0).getLicencePlate();
						lblLiencenPlate.setText(s);
						s="Producer: "+result.get(0).getProducer();
						lblProducer.setText(s);
				
					}
					
			}}
		});
	}
}
