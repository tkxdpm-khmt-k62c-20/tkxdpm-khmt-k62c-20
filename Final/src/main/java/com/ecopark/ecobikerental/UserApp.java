package com.ecopark.ecobikerental;

import com.ecopark.ecobikerental.user.home.controllers.EBRUserController;
import com.ecopark.ecobikerental.user.home.guis.EBRUser;

public class UserApp {

    public static void main(String[] args) {
        EBRUserController userController = new EBRUserController();
        new EBRUser(userController);
    }
}
