package com.ecopark.ecobikerental.admin.modules.manageStation;

import com.ecopark.ecobikerental.abstractdata.controllers.ADataPageController;
import com.ecopark.ecobikerental.abstractdata.controllers.IDataManageController;
import com.ecopark.ecobikerental.abstractdata.guis.ADataListPane;
import com.ecopark.ecobikerental.abstractdata.guis.ADataSearchPane;
import com.ecopark.ecobikerental.abstractdata.guis.ADataSinglePane;
import com.ecopark.ecobikerental.api.StationAPI;
import com.ecopark.ecobikerental.bean.Station;
import com.ecopark.ecobikerental.user.station.guis.StationSearchPane;
import com.ecopark.ecobikerental.user.station.guis.StationSinglePane;
import com.ecopark.ecobikerental.admin.modules.manageStation.AdminStationListPane;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JPanel;

public class AdminManageStationController extends ADataPageController<Station> implements IDataManageController<Station>{
	
	public AdminManageStationController() {
		super();
	}
	
	private AddStationPopUp addStationPopUp = new AddStationPopUp();
	@Override
	public ADataListPane<Station> createListPane() {
		return new AdminStationListPane();
	}
	
	@Override
	public ADataSearchPane createSearchPane() {
		return new StationSearchPane();
	}
	
	@Override
	public ADataSinglePane<Station> createSinglePane() {
		return new StationSinglePane();
	}

	@Override
    public void search(Map<String, String> searchParams) {
        ArrayList<Station> stations = new StationAPI()
        .getStations(searchParams);
        this.pagePane.getListPane().updateData(stations);
    }
	
	public void updateListStation() {
		super.searchPane.fireSearchEvent();
	}
	
	@Override
	public JPanel getDataPagePane() {
		this.pagePane = new AdminManageStationPage(super.searchPane, super.listPane);
		super.searchPane.fireSearchEvent();
		AdminManageStationPage adminPage = (AdminManageStationPage) this.pagePane;
		adminPage.setController(this);
		this.addStationPopUp.setController(this);
		return this.pagePane;
	}

	public void onBtnAddStationClick() {
		// TODO ...
		this.addStationPopUp.setVisible(true);
	}
	
	@SuppressWarnings("deprecation")
	public boolean addStation(HashMap<String,String> info) {
		Station newStation = new Station();
		
		newStation.setName(info.get("name"));
		newStation.setAddress(info.get("address"));
		newStation.setNumberOfEmptyDocks(Integer.parseInt(info.get("numberOfEmptyDocks")));

		
		boolean addState = new StationAPI().addStation(newStation);
		
		return addState;
	}

	@Override
	public boolean create(Station t) {
		// TODO Auto-generated method stub
		System.out.println("Create Station");
		return new StationAPI().addStation(t);
	}

	@Override
	public void read(Station t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Station t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(Station t) {
		// TODO Auto-generated method stub
		
	}
}
