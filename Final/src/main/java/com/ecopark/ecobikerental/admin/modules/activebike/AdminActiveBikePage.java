package com.ecopark.ecobikerental.admin.modules.activebike;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SpringLayout;

import com.ecopark.ecobikerental.abstractdata.guis.ADataListPane;
import com.ecopark.ecobikerental.abstractdata.guis.ADataPagePane;
import com.ecopark.ecobikerental.abstractdata.guis.ADataSearchPane;
import com.ecopark.ecobikerental.admin.AdminController;
import com.ecopark.ecobikerental.bean.Vehicle;

public class AdminActiveBikePage extends ADataPagePane<Vehicle>{

	AdminActiveBikeController controller;
	
	public AdminActiveBikePage(ADataSearchPane searchPane,ADataListPane<Vehicle> listPane) {
		SpringLayout layout = new SpringLayout();
		this.setLayout(layout);
		this.add(searchPane);
		this.add(listPane);
		
		JLabel lblTitle = new JLabel("Active Bike");
		lblTitle.setFont(new Font("Tahoma", Font.PLAIN, 14));
		add(lblTitle);
		
		layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, lblTitle, 0, SpringLayout.HORIZONTAL_CENTER, this);
		layout.putConstraint(SpringLayout.NORTH, lblTitle, 5, SpringLayout.NORTH, this);
		
		layout.putConstraint(SpringLayout.WEST, searchPane, 5, SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.NORTH, searchPane, 5, SpringLayout.SOUTH, lblTitle);
		layout.putConstraint(SpringLayout.EAST, searchPane, -5, SpringLayout.EAST, this);

		layout.putConstraint(SpringLayout.WEST, listPane, 5, SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.NORTH, listPane, 5, SpringLayout.SOUTH, searchPane);
		layout.putConstraint(SpringLayout.EAST, listPane, -5, SpringLayout.EAST, this);

	
		JButton btnBack = new JButton("Back");
		layout.putConstraint(SpringLayout.NORTH, btnBack, 0, SpringLayout.NORTH, this);
		layout.putConstraint(SpringLayout.WEST, btnBack, 5, SpringLayout.WEST, this);
		add(btnBack);
		
		btnBack.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				AdminController.getInstance().displayAdminHome();
			}
		});

	}
	
//	public void setController(AdminUsingBikeController adminUsingBikeController) {
//		this.controller = adminUsingBikeController;
//	}

	public void setController(AdminActiveBikeController adminActiveBikeController) {
		// TODO Auto-generated method stub
		this.controller = adminActiveBikeController;
	}
}
