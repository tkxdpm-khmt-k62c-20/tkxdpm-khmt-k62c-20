package com.ecopark.ecobikerental.admin.modules.activebike;

import javax.swing.BorderFactory;

import com.ecopark.ecobikerental.abstractdata.controllers.ADataPageController;
import com.ecopark.ecobikerental.abstractdata.guis.ADataListPane;
import com.ecopark.ecobikerental.abstractdata.guis.ADataSinglePane;
import com.ecopark.ecobikerental.bean.Vehicle;

public class ActiveBikeListPane extends ADataListPane<Vehicle>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ActiveBikeListPane() {
		super();
	}
	public ActiveBikeListPane(ADataPageController<Vehicle> controller ) {
		this.controller = controller;
	}
	@Override
	public void decorateSinglePane(ADataSinglePane<Vehicle> singlePane) {
		singlePane.setBorder(BorderFactory.createRaisedBevelBorder());
	}

}