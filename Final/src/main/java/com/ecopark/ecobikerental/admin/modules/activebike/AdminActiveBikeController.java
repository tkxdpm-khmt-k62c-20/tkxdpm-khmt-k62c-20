package com.ecopark.ecobikerental.admin.modules.activebike;

import java.util.Map;

import javax.swing.JPanel;

import com.ecopark.ecobikerental.abstractdata.controllers.ADataPageController;
import com.ecopark.ecobikerental.abstractdata.controllers.IDataManageController;
import com.ecopark.ecobikerental.abstractdata.guis.ADataListPane;
import com.ecopark.ecobikerental.abstractdata.guis.ADataSearchPane;
import com.ecopark.ecobikerental.abstractdata.guis.ADataSinglePane;
import com.ecopark.ecobikerental.admin.modules.managebike.AdminBikeListPane;
import com.ecopark.ecobikerental.admin.modules.managebike.AdminBikeSearchPane;
import com.ecopark.ecobikerental.admin.modules.managebike.AdminManageBikePage;
import com.ecopark.ecobikerental.admin.modules.managebike.BikeSinglePane;
import com.ecopark.ecobikerental.api.ServerAPI;
import com.ecopark.ecobikerental.bean.Vehicle;

public class AdminActiveBikeController extends ADataPageController<Vehicle> implements IDataManageController<Vehicle> {
	
	@Override
	public ADataListPane<Vehicle> createListPane() {
		return new ActiveBikeListPane(this);
	}

	@Override
	public ADataSearchPane createSearchPane() {
		return new ActiveBikeSearchPane();
	}

	@Override
	public ADataSinglePane<Vehicle> createSinglePane() {
		return new ActiveBikeSinglePane();
	}

	@Override
	public void search(Map<String, String> searchParams) {
		super.listPane.updateData(new ServerAPI().getAllActiveBike(searchParams));
	}

	@Override
	public JPanel getDataPagePane() {
		this.pagePane = new AdminActiveBikePage(super.searchPane, super.listPane);
		super.searchPane.fireSearchEvent();
		AdminActiveBikePage adminPage = (AdminActiveBikePage) this.pagePane;
		adminPage.setController(this);
		return this.pagePane;
	}
	
	
	
	@Override
	public boolean create(Vehicle t) {
//		// TODO Auto-generated method stub
//		System.out.println("Create Bike");
//		r
		return false;
	}

	@Override
	public void delete(Vehicle t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void read(Vehicle t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(Vehicle t) {
		// TODO Auto-generated method stub
		
	}
}
