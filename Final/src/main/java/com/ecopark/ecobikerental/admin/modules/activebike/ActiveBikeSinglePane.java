package com.ecopark.ecobikerental.admin.modules.activebike;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import com.ecopark.ecobikerental.abstractdata.guis.ADataSinglePane;
import com.ecopark.ecobikerental.bean.Bike;
import com.ecopark.ecobikerental.bean.EBike;
import com.ecopark.ecobikerental.bean.Vehicle;

public class ActiveBikeSinglePane extends ADataSinglePane<Vehicle> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel lblID;
	private JLabel lblName;
	private JLabel lblType;
	private JLabel lblStation;
	private JLabel lblCost;
	private JLabel lblWeight;
	private JLabel lblDate;
	private JLabel lblProducer;
	
	public ActiveBikeSinglePane() {
		
		lblID = new JLabel("Id");
		lblID.setHorizontalAlignment(SwingConstants.CENTER);
		lblID.setPreferredSize(new Dimension(100,20));
		lblID.setBorder(BorderFactory.createLineBorder(Color.yellow));
		GridBagConstraints gbc_lblID = new GridBagConstraints();
		gbc_lblID.insets = new Insets(0, 0, 0, 5);
		gbc_lblID.gridx = 0;
		gbc_lblID.gridy = 0;
		add(lblID, gbc_lblID);
		
		lblName = new JLabel("name");
		lblName.setHorizontalAlignment(SwingConstants.CENTER);
		lblName.setPreferredSize(new Dimension(100,20));
		lblName.setBorder(BorderFactory.createLineBorder(Color.black));
		GridBagConstraints gbc_lblName = new GridBagConstraints();
		gbc_lblName.fill = GridBagConstraints.BOTH;
		gbc_lblName.insets = new Insets(0, 0, 0, 5);
		gbc_lblName.gridx = 1;
		gbc_lblName.gridy = 0;
		add(lblName, gbc_lblName);
		
		lblType = new JLabel("Type");
		lblType.setHorizontalAlignment(SwingConstants.CENTER);
		lblType.setPreferredSize(new Dimension(100,20));
		lblType.setBorder(BorderFactory.createLineBorder(Color.black));
		GridBagConstraints gbc_lblType = new GridBagConstraints();
		gbc_lblType.insets = new Insets(0, 0, 0, 5);
		gbc_lblType.gridx = 2;
		gbc_lblType.gridy = 0;
		add(lblType, gbc_lblType);
		
		lblStation = new JLabel("station");
		lblStation.setHorizontalAlignment(SwingConstants.CENTER);
		lblStation.setPreferredSize(new Dimension(100,20));
		lblStation.setBorder(BorderFactory.createLineBorder(Color.black));
		GridBagConstraints gbc_lblStation = new GridBagConstraints();
		gbc_lblStation.insets = new Insets(0, 0, 0, 5);
		gbc_lblStation.gridx = 3;
		gbc_lblStation.gridy = 0;
		add(lblStation, gbc_lblStation);
		
		lblCost = new JLabel("cost");
		lblCost.setHorizontalAlignment(SwingConstants.CENTER);
		lblCost.setPreferredSize(new Dimension(100,20));
		lblCost.setBorder(BorderFactory.createLineBorder(Color.black));
		GridBagConstraints gbc_lblCost = new GridBagConstraints();
		gbc_lblCost.insets = new Insets(0, 0, 0, 5);
		gbc_lblCost.gridx = 4;
		gbc_lblCost.gridy = 0;
		add(lblCost, gbc_lblCost);
		
		lblWeight = new JLabel("weight");
		lblWeight.setHorizontalAlignment(SwingConstants.CENTER);
		lblWeight.setPreferredSize(new Dimension(100,20));
		lblWeight.setBorder(BorderFactory.createLineBorder(Color.black));
		GridBagConstraints gbc_lblWeight = new GridBagConstraints();
		gbc_lblWeight.insets = new Insets(0, 0, 0, 5);
		gbc_lblWeight.gridx = 5;
		gbc_lblWeight.gridy = 0;
		add(lblWeight, gbc_lblWeight);
		
		lblDate = new JLabel("manufacturing date");
		lblDate.setHorizontalAlignment(SwingConstants.CENTER);
		lblDate.setPreferredSize(new Dimension(100,20));
		lblDate.setBorder(BorderFactory.createLineBorder(Color.black));
		GridBagConstraints gbc_lblDate = new GridBagConstraints();
		gbc_lblDate.insets = new Insets(0, 0, 0, 5);
		gbc_lblDate.gridx = 6;
		gbc_lblDate.gridy = 0;
		add(lblDate, gbc_lblDate);
		
		lblProducer = new JLabel("producer");
		lblProducer.setHorizontalAlignment(SwingConstants.CENTER);
		lblProducer.setPreferredSize(new Dimension(120,20));
		lblProducer.setBorder(BorderFactory.createLineBorder(Color.black));
		GridBagConstraints gbc_lblProducer = new GridBagConstraints();
		gbc_lblProducer.anchor = GridBagConstraints.EAST;
		gbc_lblProducer.gridx = 7;
		gbc_lblProducer.gridy = 0;
		add(lblProducer, gbc_lblProducer);
		this.setBorder(BorderFactory.createRaisedSoftBevelBorder());
	}

	@SuppressWarnings("deprecation")
	@Override
	public void displayData() {
		// TODO Auto-generated method stub
		if (t instanceof Vehicle) {
			Vehicle vehicle = (Vehicle) t;
			System.out.println(t.toString());
			lblID.setText(String.format("ID:%s", vehicle.getId()));
			lblName.setText(String.format("Name:%s", vehicle.getName()));
			lblStation.setText(String.format("Station:%s", vehicle.getStationId()));
			if (vehicle.getManufacturingDate()!=null)
				lblDate.setText(String.format("Date:%d/%d/%d", vehicle.getManufacturingDate().getDay(),
					vehicle.getManufacturingDate().getMonth(),vehicle.getManufacturingDate().getYear()));
			else lblDate.setText("Date null");
			lblProducer.setText(String.format("Pd:%s", vehicle.getProducer()));
			lblCost.setText(String.format("Cost: %d",vehicle.getCost()));
			lblWeight.setText(String.format("Weight:%.1f", vehicle.getWeight()));
		}
		if (t instanceof Bike) {
			lblType.setText("Type:Bike");
		}
		else if (t instanceof EBike) {
			lblType.setText("Type:EBike");
		}
		else {
			lblType.setText("Type:Twin Bike");
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

}
