package com.ecopark.ecobikerental.admin.modules.manageStation;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Dialog.ModalExclusionType;
import java.awt.Window.Type;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.SpinnerListModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.text.NumberFormatter;

import com.ecopark.ecobikerental.admin.modules.managebike.AddBikePopUp;

public class AddStationPopUp extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField idField;
	private JTextField typeField;
	private JTextField nameField;
	private JTextField addressField;
	private JTextField numberBikeField;
	private JTextField numberEbikeField;
	private JTextField numberTwinbikeField;
	private JTextField numberOfEmptyDocksField;
	private JTextField distanceToCustomerField;
	private AdminManageStationController controller;
	private JLabel lbError;

	private HashMap<String, String> getInput() throws ParseException {
		HashMap<String, String> input = new HashMap<String, String>();

		Number number;

//		input.put("id", idField.getText().trim());
//		input.put("type", typeField.getText().trim());
		input.put("name", nameField.getText().trim());
		input.put("address", addressField.getText().trim());
//		if (!numberBikeField.getText().trim().equals("")) {
//			number = NumberFormat.getInstance().parse(numberBikeField.getText().trim());
//			input.put("numberOfBikes", number.toString());
//		}
//		if (!numberEbikeField.getText().trim().equals("")) {
//			number = NumberFormat.getInstance().parse(numberEbikeField.getText().trim());
//			input.put("numberOfEBikes", number.toString());
//		}
//		if (!numberTwinbikeField.getText().trim().equals("")) {
//			number = NumberFormat.getInstance().parse(numberTwinbikeField.getText().trim());
//			input.put("numberOfTwinBikes", number.toString());
//		}
		if (!numberOfEmptyDocksField.getText().trim().equals("")) {
			number = NumberFormat.getInstance().parse(numberOfEmptyDocksField.getText().trim());
			input.put("numberOfEmptyDocks", number.toString());
		}
//		if (!distanceToCustomerField.getText().trim().equals("")) {
//			number = NumberFormat.getInstance().parse(distanceToCustomerField.getText().trim());
//			input.put("distanceToCustomer", number.toString());
//		}

		return input;
	}

	/**
	 * Create the dialog.
	 */
	public AddStationPopUp() {
		setFont(new Font("Dialog", Font.PLAIN, 14));
		setTitle("Add Station");
		getContentPane().setBackground(new Color(39, 217, 51));
		setBackground(new Color(0, 153, 255));
		setForeground(new Color(102, 204, 255));
		setModalExclusionType(ModalExclusionType.APPLICATION_EXCLUDE);
		setResizable(false);
		getContentPane().setFont(new Font("Tahoma", Font.PLAIN, 12));
		setType(Type.POPUP);
		setAlwaysOnTop(true);
		setBounds(100, 100, 615, 298);
		BorderLayout borderLayout = new BorderLayout();
		borderLayout.setVgap(10);
		getContentPane().setLayout(borderLayout);
		contentPanel.setBackground(new Color(39, 217, 51));
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.NORTH);
		contentPanel.setLayout(new GridLayout(0, 4, 5, 10));

		NumberFormat format = NumberFormat.getInstance();
		NumberFormatter formatter = new NumberFormatter(format);
		formatter.setValueClass(Integer.class);
		formatter.setMinimum(0);
		formatter.setMaximum(Integer.MAX_VALUE);
		formatter.setAllowsInvalid(true);
		{
			JLabel lblNewLabel_1 = new JLabel("Name");
			lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
			contentPanel.add(lblNewLabel_1);
		}
		{
			nameField = new JTextField();
			nameField.setFont(new Font("Tahoma", Font.PLAIN, 14));
			contentPanel.add(nameField);
			nameField.setColumns(10);
		}
		{
			JLabel lblNewLabel_2 = new JLabel("Address");
			lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
			contentPanel.add(lblNewLabel_2);
		}
		{
			addressField = new JTextField();
			addressField.setFont(new Font("Tahoma", Font.PLAIN, 14));
			contentPanel.add(addressField);
			addressField.setColumns(10);
		}
		{
			JLabel lblNewLabel_7 = new JLabel("Number of Empty Docks");
			lblNewLabel_7.setFont(new Font("Tahoma", Font.PLAIN, 14));
			contentPanel.add(lblNewLabel_7);
		}
		{
			numberOfEmptyDocksField = new JFormattedTextField(formatter);
			numberOfEmptyDocksField.setFont(new Font("Tahoma", Font.PLAIN, 14));
			contentPanel.add(numberOfEmptyDocksField);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
				okButton.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						AddStationPopUp.this.onBtnOkClick();

					}
				});
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				cancelButton.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						AddStationPopUp.this.setVisible(false);
					}

				});
				buttonPane.add(cancelButton);
			}
		}
		{
			lbError = new JLabel("");
			lbError.setForeground(new Color(255, 51, 51));
			lbError.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lbError.setHorizontalAlignment(SwingConstants.CENTER);
			getContentPane().add(lbError, BorderLayout.CENTER);
		}
	}

	public void setController(AdminManageStationController controller) {
		this.controller = controller;
	}

	private void onBtnOkClick() {
		HashMap<String, String> input = null;
		try {
			input = this.getInput();
		} catch (ParseException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		if (!(input == null) && this.validateInput(input)) {
			if (this.controller.addStation(input)) {
				this.setVisible(false);
				this.controller.updateListStation();
				System.out.println("Add station success");
			} else {
				this.showError("Server response false");
			}
		} else {
			this.showError("Cần nhập đủ thông tin!");
		}
	}

	private boolean validateInput(HashMap<String, String> input) {

		for (String key : input.keySet()) {
			if (input.get(key).equals("")) {
				return false;
			}
		}
		return true;
	}

	private void showError(String error) {
		this.lbError.setText(error);
		int delay = 5000; // milliseconds
		ActionListener taskPerformer = new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				AddStationPopUp.this.lbError.setText("");
			}
		};
		new javax.swing.Timer(delay, taskPerformer).start();
	}
}
