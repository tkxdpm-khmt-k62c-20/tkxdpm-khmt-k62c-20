package com.ecopark.ecobikerental.admin.modules.manageStation;

import com.ecopark.ecobikerental.abstractdata.guis.ADataListPane;
import com.ecopark.ecobikerental.abstractdata.guis.ADataSinglePane;
import com.ecopark.ecobikerental.bean.Station;

@SuppressWarnings("serial")
public class AdminStationListPane extends ADataListPane<Station>{
	
	
	public AdminStationListPane() {
		super();
	}
	
	@Override
	public void decorateSinglePane(ADataSinglePane<Station> singlePane) {}
}
