package com.ecopark.ecobikerental.admin.modules.manageStation;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SpringLayout;

import com.ecopark.ecobikerental.abstractdata.guis.ADataListPane;
import com.ecopark.ecobikerental.abstractdata.guis.ADataPagePane;
import com.ecopark.ecobikerental.abstractdata.guis.ADataSearchPane;
import com.ecopark.ecobikerental.admin.AdminController;
import com.ecopark.ecobikerental.bean.Station;

@SuppressWarnings("serial")
public class AdminManageStationPage extends ADataPagePane<Station> {

	AdminManageStationController controller;
	JButton btnAddStation;

	public AdminManageStationPage() {
		super();
	}

	public AdminManageStationPage(ADataSearchPane searchPane, ADataListPane<Station> stationListPane) {
		super(searchPane, stationListPane);

		SpringLayout layout = new SpringLayout();
		this.setLayout(layout);
		this.add(searchPane);
		this.add(stationListPane);
		btnAddStation = new JButton("Add station");
		this.add(btnAddStation);
		
		JLabel lblTitle = new JLabel("Manage Station");
		lblTitle.setFont(new Font("Tahoma", Font.BOLD, 14));
		add(lblTitle);
		
		layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, lblTitle, 0, SpringLayout.HORIZONTAL_CENTER, this);
		layout.putConstraint(SpringLayout.NORTH, lblTitle, 5, SpringLayout.NORTH, this);
		
		layout.putConstraint(SpringLayout.WEST, searchPane, 5, SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.NORTH, searchPane, 5, SpringLayout.SOUTH, lblTitle);
		layout.putConstraint(SpringLayout.EAST, searchPane, -5, SpringLayout.EAST, this);

		layout.putConstraint(SpringLayout.WEST, stationListPane, 5, SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.NORTH, stationListPane, 5, SpringLayout.SOUTH, searchPane);
		layout.putConstraint(SpringLayout.EAST, stationListPane, -5, SpringLayout.EAST, this);
		layout.putConstraint(SpringLayout.SOUTH, stationListPane, -5, SpringLayout.NORTH, btnAddStation);
		
		layout.putConstraint(SpringLayout.SOUTH, btnAddStation, -5, SpringLayout.SOUTH, this);
		layout.putConstraint(SpringLayout.HORIZONTAL_CENTER,btnAddStation,0,SpringLayout.HORIZONTAL_CENTER,this);
		
		JButton btnBack = new JButton("Back");
		layout.putConstraint(SpringLayout.NORTH, btnBack, 0, SpringLayout.NORTH, this);
		layout.putConstraint(SpringLayout.WEST, btnBack, 5, SpringLayout.WEST, this);
		add(btnBack);
		
		btnBack.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				AdminController.getInstance().displayAdminHome();
			}
		});
	}

	public void setController(AdminManageStationController adminManageStationController) {
		this.controller = adminManageStationController;
		this.btnAddStation.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				AdminManageStationPage.this.controller.onBtnAddStationClick();
			}
		});
	}
}
