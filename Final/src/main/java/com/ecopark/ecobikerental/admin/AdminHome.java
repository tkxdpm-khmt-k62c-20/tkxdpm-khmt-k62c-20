package com.ecopark.ecobikerental.admin;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class AdminHome extends JFrame {

	private JPanel contentPane;


	/**
	 * Create the frame.
	 */
	public AdminHome() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblTitle = new JLabel("Admin Home");
		lblTitle.setFont(new Font("Tahoma", Font.BOLD, 30));
		lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitle.setBounds(370, 10, 250, 50);
		contentPane.add(lblTitle);
		
		JButton btnManageStation = new JButton("Quản lý bãi xe");
		btnManageStation.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnManageStation.setBounds(96, 200, 200, 50);
		contentPane.add(btnManageStation);
		btnManageStation.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				AdminController.getInstance().manageStation();
				
			}
		});
		
		JButton btnManageBike = new JButton("Quản lý xe");
		btnManageBike.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnManageBike.setBounds(368, 200, 200, 50);
		contentPane.add(btnManageBike);
		btnManageBike.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				AdminController.getInstance().manageBike();
				
			}
		});
		
		JButton btnActiveBike = new JButton("Quản lý xe đang sử dụng");
		btnActiveBike.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnActiveBike.setBounds(630, 200, 250, 50);
		contentPane.add(btnActiveBike);
		btnActiveBike.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				AdminController.getInstance().activeBike();
			}
		});
		
	}
}
