package com.ecopark.ecobikerental.admin;

import javax.swing.JFrame;

import com.ecopark.ecobikerental.admin.modules.activebike.AdminActiveBikeController;
import com.ecopark.ecobikerental.admin.modules.activebike.AdminActiveBikePage;
import com.ecopark.ecobikerental.admin.modules.manageStation.AdminManageStationController;
import com.ecopark.ecobikerental.admin.modules.managebike.AdminManageBikeController;

public class AdminController {
	public final Integer DEFAULT_SCREEN_WIDTH = 1000;
	public final Integer DEFAULT_SCREEN_HEIGHT = 600;
	private static AdminController instance = null;
	private AdminHome homePage;
	private AdminManageStationController manageStationController;
	private AdminManageBikeController manageBikeController;
	private AdminActiveBikeController activeBikeController;
	private JFrame adminManageStationPage;
	private JFrame adminManageBikePage;
	private JFrame adminActiveBikePage;

	public synchronized static AdminController getInstance() {
		if (instance == null) {
			instance = new AdminController();
		}
		return instance;
	}

	private AdminController() {
		this.homePage = new AdminHome();
		this.homePage.setBounds(100, 100, this.DEFAULT_SCREEN_WIDTH, this.DEFAULT_SCREEN_HEIGHT);
		this.manageStationController = new AdminManageStationController();
		this.manageBikeController = new AdminManageBikeController();
		this.activeBikeController = new AdminActiveBikeController();

		// managestation page
		this.adminManageStationPage = new JFrame();
		this.adminManageStationPage.setBounds(100, 100, this.DEFAULT_SCREEN_WIDTH, this.DEFAULT_SCREEN_HEIGHT);
		this.adminManageStationPage.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.adminManageStationPage.add(this.manageStationController.getDataPagePane());

		// managebike page
		this.adminManageBikePage = new JFrame();
		this.adminManageBikePage.setBounds(100, 100, this.DEFAULT_SCREEN_WIDTH, this.DEFAULT_SCREEN_HEIGHT);
		this.adminManageBikePage.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.adminManageBikePage.add(this.manageBikeController.getDataPagePane());

		// usingbike page
		this.adminActiveBikePage = new JFrame();
		this.adminActiveBikePage.setBounds(100, 100, this.DEFAULT_SCREEN_WIDTH, this.DEFAULT_SCREEN_HEIGHT);
		this.adminActiveBikePage.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.adminActiveBikePage.add(this.activeBikeController.getDataPagePane());
	}

	public void displayAdminHome() {
		this.adminManageStationPage.setVisible(false);
		this.adminManageBikePage.setVisible(false);
		this.adminActiveBikePage.setVisible(false);
		this.homePage.setVisible(true);

	}

	public void manageStation() {
		this.homePage.setVisible(false);
		this.adminManageStationPage.setVisible(true);
	}

	public void manageBike() {
		this.homePage.setVisible(false);
		this.adminManageBikePage.setVisible(true);
	}

	public void activeBike() {
		this.homePage.setVisible(false);
		this.adminActiveBikePage.setVisible(true);
	}

	public void manageBikeToActiveBike() {
		this.adminManageBikePage.setVisible(false);
		this.adminActiveBikePage.setVisible(true);
	}
}
