package com.ecopark.ecobikerental.admin.modules.managebike;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SpringLayout;

import com.ecopark.ecobikerental.abstractdata.guis.ADataListPane;
import com.ecopark.ecobikerental.abstractdata.guis.ADataPagePane;
import com.ecopark.ecobikerental.abstractdata.guis.ADataSearchPane;
import com.ecopark.ecobikerental.admin.AdminController;
import com.ecopark.ecobikerental.bean.Vehicle;

public class AdminManageBikePage extends ADataPagePane<Vehicle>{

	AdminManageBikeController controller;
	JButton btnAddBike;
	
	public AdminManageBikePage(ADataSearchPane searchPane,ADataListPane<Vehicle> listPane) {
		SpringLayout layout = new SpringLayout();
		this.setLayout(layout);
		this.add(searchPane);
		this.add(listPane);
		btnAddBike = new JButton("Add bike");
		this.add(btnAddBike);
		
		JLabel lblTitle = new JLabel("Manage Bike");
		lblTitle.setFont(new Font("Tahoma", Font.PLAIN, 14));
		add(lblTitle);
		
		layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, lblTitle, 0, SpringLayout.HORIZONTAL_CENTER, this);
		layout.putConstraint(SpringLayout.NORTH, lblTitle, 5, SpringLayout.NORTH, this);
		
		layout.putConstraint(SpringLayout.WEST, searchPane, 5, SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.NORTH, searchPane, 5, SpringLayout.SOUTH, lblTitle);
		layout.putConstraint(SpringLayout.EAST, searchPane, -5, SpringLayout.EAST, this);

		layout.putConstraint(SpringLayout.WEST, listPane, 5, SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.NORTH, listPane, 5, SpringLayout.SOUTH, searchPane);
		layout.putConstraint(SpringLayout.EAST, listPane, -5, SpringLayout.EAST, this);
		layout.putConstraint(SpringLayout.SOUTH, listPane, -5, SpringLayout.NORTH, btnAddBike);
		
		layout.putConstraint(SpringLayout.SOUTH, btnAddBike, -5, SpringLayout.SOUTH, this);
		layout.putConstraint(SpringLayout.HORIZONTAL_CENTER,btnAddBike,0,SpringLayout.HORIZONTAL_CENTER,this);
		
		JButton btnBack = new JButton("Back");
		layout.putConstraint(SpringLayout.NORTH, btnBack, 0, SpringLayout.NORTH, this);
		layout.putConstraint(SpringLayout.WEST, btnBack, 5, SpringLayout.WEST, this);
		add(btnBack);
		
		JButton btnGoActiveBike = new JButton("Manage Active Bike");
		layout.putConstraint(SpringLayout.SOUTH, btnGoActiveBike, 0, SpringLayout.SOUTH, lblTitle);
		layout.putConstraint(SpringLayout.EAST, btnGoActiveBike, 0, SpringLayout.EAST, this);
		add(btnGoActiveBike);
		
		btnBack.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				AdminController.getInstance().displayAdminHome();
			}
		});
		
		btnGoActiveBike.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				AdminController.getInstance().manageBikeToActiveBike();
			}
		});

	}
	
	public void setController(AdminManageBikeController adminManageBikeController) {
		this.controller = adminManageBikeController;
		this.btnAddBike.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				AdminManageBikePage.this.controller.onBtnAddBikeClick();
			}
		});
	}
}
