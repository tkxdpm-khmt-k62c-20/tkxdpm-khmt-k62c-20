package com.ecopark.ecobikerental.admin.modules.managebike;

import javax.swing.BorderFactory;

import com.ecopark.ecobikerental.abstractdata.controllers.ADataPageController;
import com.ecopark.ecobikerental.abstractdata.guis.ADataListPane;
import com.ecopark.ecobikerental.abstractdata.guis.ADataSinglePane;
import com.ecopark.ecobikerental.bean.Vehicle;

public class AdminBikeListPane extends ADataListPane<Vehicle>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AdminBikeListPane() {
		super();
	}
	public AdminBikeListPane(ADataPageController<Vehicle> controller ) {
		this.controller = controller;
	}
	@Override
	public void decorateSinglePane(ADataSinglePane<Vehicle> singlePane) {
		singlePane.setBorder(BorderFactory.createRaisedBevelBorder());
	}

}
