package com.ecopark.ecobikerental;

import com.ecopark.ecobikerental.admin.AdminController;
import com.ecopark.ecobikerental.bean.User;

public class AdminApp {
	public static void main(String args[]) {
		AdminController.getInstance().displayAdminHome();
	}
}
