package com.ecopark.ecobikerental.station.guis;

import static org.junit.Assert.assertTrue;

import java.awt.Component;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JTextField;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.ecopark.ecobikerental.user.station.guis.StationSearchPane;

@RunWith(Parameterized.class)
public class GetQueryParamsTest {

    private StationSearchPane stationSearchPane;
    private Map<String, String> inputValues;
    private Map<String, String> expectedResult;

    @Before
    public void initialize() {
        this.stationSearchPane = new StationSearchPane();

        for (Component component : stationSearchPane.getComponents()) {
            if (component instanceof JTextField) {
                JTextField textField = (JTextField) component;
                String componentName = component.getName();
                textField.setText(this.inputValues.get(componentName));
            }
        }
    }

    @Parameterized.Parameters
    public static Collection<Object> namesAndAddresses() {
        ArrayList<Object> testCases = new ArrayList<>();
        String[] inputNames = { "niar" };
        String[] inputAddresses = { "hn" };
        String[] expectedNames = { "niar" };
        String[] expectedAddresses = { "hn" };

        int numberOfTestCases = inputNames.length;

        for (int i = 0; i < numberOfTestCases; ++i) {
            Map<String, String> expectedResult = new HashMap<>();
            expectedResult.put("name", expectedNames[i]);
            expectedResult.put("address", expectedAddresses[i]);

            Map<String, String> inputValues = new HashMap<>();
            inputValues.put("name", inputNames[i]);
            inputValues.put("address", inputAddresses[i]);

            testCases.add(new Object[] { inputValues, expectedResult });
        }

        return testCases;
    }

    public GetQueryParamsTest(
        Map<String, String> inputValues,
        Map<String, String> expectedResult
    ) {
        this.inputValues = inputValues;
        this.expectedResult = expectedResult;
    }

    @Test
    public void testGetQueryParams() {
        assertTrue(
            expectedResult.equals(this.stationSearchPane.getQueryParams())
        );
    }
}
