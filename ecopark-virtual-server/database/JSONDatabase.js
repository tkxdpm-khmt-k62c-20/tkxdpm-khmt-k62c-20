const Seed = require("./seed/seed");

class JSONDatabase {
  constructor() {
    const seed = new Seed();
    this._stations = seed.getStations();
    this._vehicles = seed.getVehicles();
    this._rentingVehicles = seed.getRentingVehicles();
  }

  searchStations({ nameParam = "", addressParam = "" } = {}) {
    return this._stations.filter((station) => {
      const { name, address } = station;

      if (name.match(nameParam) === null) {
        return false;
      }
      if (address.match(addressParam) === null) {
        return false;
      }

      return true;
    });
  }

  createStations(station) {
    station.id = `station${this._stations.length + 1}`;
    station.numberOfBikes = 0;
    station.numberOfEBikes = 0;
    station.numberOfTwinBikes = 0;
    this._stations.push(station);
  }

  searchVehicles({
    idParam = "",
    typeParam = "",
    nameParam = "",
    producerParam = "",
  } = {}) {
    return this._vehicles.filter((vehicle) => {
      const { id, type, name, producer } = vehicle;

      if (id.match(idParam) === null) {
        return false;
      }
      if (type.match(typeParam) === null) {
        return false;
      }
      if (name.match(nameParam) === null) {
        return false;
      }
      if (producer.match(producerParam) === null) {
        return false;
      }

      return true;
    });
  }

  createVehicles(vehicle) {
    const { type } = vehicle;
    vehicle.id = `${type}${this._vehicles.length + 1}`;
    this._vehicles.push(vehicle);
  }

  searchRentingVehicles({ vehicleIdParam, accountNumberParam = 0 }) {
    const result = this._rentingVehicles.filter(
      ({ vehicleId, accountNumber }) => {
        return (
          (vehicleIdParam !== undefined && vehicleId.match(vehicleIdParam)) ||
          accountNumber === accountNumberParam
        );
      }
    );
    return result;
  }

  createRentingVehicleLine(rentedVehicleLine) {
    rentedVehicleLine.rentedTime = new Date();
    this._rentingVehicles.push(rentedVehicleLine);
  }

  getActiveBike({ typeParam = "", nameParam = "", producerParam = "" } = {}) {
    // let vehicles = this._vehicles.filter(x => x.status === true);
    // return vehicles;
    return this._vehicles.filter((vehicle) => {
      const { type, name, producer } = vehicle;
      if(vehicle.status !== true) {
        return false;
      }
      if (type.match(typeParam) === null) {
        return false;
      }
      if (name.match(nameParam) === null) {
        return false;
      }
      if (producer.match(producerParam) === null) {
        return false;
      }
      return true;
    });
  }
}

const database = new JSONDatabase();
module.exports = database;
