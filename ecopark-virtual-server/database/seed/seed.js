const fs = require("fs");
const path = require("path");

class Seed {
  constructor() {
    this._stations = this.readDataFromFile(
      path.join(__dirname, "stations.json")
    );
    this._vehicles = this.readDataFromFile(
      path.join(__dirname, "vehicles.json")
    );
    this._rentingVehicles = this.readDataFromFile(
      path.join(__dirname, "renting-vehicles.json")
    );
  }

  readDataFromFile(filepath) {
    const rawdata = fs.readFileSync(filepath);
    return JSON.parse(rawdata);
  }

  getStations() {
    return this._stations;
  }

  getVehicles() {
    return this._vehicles;
  }

  getRentingVehicles() {
    return this._rentingVehicles;
  }
}

module.exports = Seed;
