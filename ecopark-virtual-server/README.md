# Ecopark virtual server

---

## Chạy server

- Yêu cầu: nodejs & npm.
- Chạy các lệnh sau:

```
npm i
npm start
```

## Các APIs

Server host: `localhost:3000`

#### Tìm kiếm bãi xe

- Phương thức: `GET`
- Đường dẫn: `/stations`
- Tham số:

  - `name`: string
  - `address`: string

- Dữ liệu trả về:

```
[
    {
        "id": "station01",
        "type": "station",
        "name": "D3",
        "address": "Công viên Hồ Thiên Nga",
        "numberOfBikes": 12,
        "numberOfEBikes": 14,
        "numberOfTwinBikes": 16,
        "numberOfEmptyDocks": 18,
        "distanceToCustomer": 342
    },
    ...
```

#### Tìm kiếm xe

- Phương thức: `GET`
- Đường dẫn: `/vehicles`
- Tham số:
  - `type`: string
  - `id`: string
  - `name`: string
  - `producer`: string
- Dữ liệu trả về: danh sách xe

```
[
    {
        "type": "bike",
        "id": "bike01",
        "name": "Bike 01",
        "stationId": "station01",
        "weight": 50,
        "licencePlate": "12345",
        "manufacturingDate": "20/11/2020",
        "producer": "producer01",
        "cost": 4000000
    },
    ...
]
```

#### Thêm xe

- Phương thức: `POST`
- Đường dẫn: `/vehicles`
- Post data:
  - `type`: string, bắt buộc
  - `name`: string, bắt buộc
  - `producer`: string, bắt buộc
  - ...
- Dữ liệu trả về:
  - Thành công: status code `201`
  - Thất bại: status code `403` & `error messages`

#### Danh sách xe đang thuê

- Phương thức: `GET`
- Đường dẫn: `/vehicles/renting-vehicles`
- Tham sỗ:
  - `vehicleId`: string
- Dữ liệu trả về:

```
[
    {
        "userId": "user1",
        "vehicleId": "bike01",
        "accountNumber": "1111222233334444",
        "rentedTime": "Mon Dec 21 2020 10:07:48 GMT+0700 (Indochina Time)"
    },
    ...
]
```

#### Kiểm tra số tài khoản ngân hàng có tồn tại?

- Phương thức: `GET`
- Đường dẫn: `/check-bank-account`
- Tham số:
  - `accountNumber`: string
- Dữ liệu trả về:
  - Có tồn tại: `Account number valid`
  - Không tồn tại: `Account number not existing`

#### Thuê xe

- Phương thức: `GET`
- Đường dẫn: `/vehicles/rent`
- Tham số:
  - `userId`: string, bắt buộc.
  - `vehicleId`: string, bắt buộc.
  - `accountNumber`: string, bắt buộc.
- Dữ liệu trả về:
  - Thành công: `201`
  - Thất bại:
    - Account number không tồn tại: `1`
    - Account number đã thuê xe khác: `2`
    - Xe đã được thuê: `3`
    - Tài khoàn không đủ: `4`
- Tham số hợp lệ:
  - `vehicleId`: khác `bike01`
  - `accountNumber`:
    - `4444333322221111`,
    - `1234123412341234`,
    - `4321432143214321`,
    - `9876543210987654`,
    - `1234567812345678`
- Tham số không hợp lệ:
  - `vehicleId`: `bike01` xe đã thuê.
  - `accountNumber`:
    - `1234567890123456`, `1111222233334444`: Tài khoản không đủ (< 400.000 VND)
