const paygate = require("./paygate");
const database = require("./database/JSONDatabase");
const { errorCodes } = require("./constants");

function validateVehicle(vehicle) {
  const errorMessages = [];
  const { type, name, producer } = vehicle;
  const validVehicleTypes = ["bike", "ebike", "twinbike"];

  if (validVehicleTypes.includes(type)) {
    if (name === undefined) {
      errorMessages.push("Vehicle name required.");
    }
    if (producer === undefined) {
      errorMessages.push("Vehicle producer required");
    }
  } else {
    errorMessages.push("Vehicle type not valid.");
  }

  return errorMessages;
}

function validateBankAccountNumber(vehicleId, accountNumber) {
  let { errorCode, balance } = paygate.getBalance(accountNumber);

  if (errorCode === undefined) {
    if (
      database.searchRentingVehicles({ accountNumberParam: accountNumber })
        .length > 0
    ) {
      errorCode = errorCodes.bankAccountNumberUsed;
    } else if (
      database.searchRentingVehicles({ vehicleIdParam: vehicleId }).length > 0
    ) {
      errorCode = errorCodes.vehicleRented;
    } else if (balance < 400000) {
      errorCode = errorCodes.balanceNotEnough;
    } else {
      errorCode = -1;
    }
  }

  return errorCode;
}

module.exports = { validateVehicle, validateBankAccountNumber };
