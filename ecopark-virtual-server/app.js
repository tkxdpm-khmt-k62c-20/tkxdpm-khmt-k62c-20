const express = require("express");
const bodyParser = require("body-parser");
const database = require("./database/JSONDatabase");
const { validateVehicle, validateBankAccountNumber } = require("./validators");
const paygate = require("./paygate");

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const PORT = 3000;

app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.get("/stations", (req, res) => {
  const { name: nameParam, address: addressParam } = req.query;
  res.json(database.searchStations({ nameParam, addressParam }));
});

app.get("/vehicles", (req, res) => {
  const {
    vehicleId: idParam,
    type: typeParam,
    name: nameParam,
    address: addressParam,
  } = req.query;
  res.json(
    database.searchVehicles({
      idParam,
      typeParam,
      nameParam,
      addressParam,
    })
  );
});

app.get("/vehicles/active", (req, res) => {
  const { type: typeParam, name: nameParam, address: addressParam } = req.query;
  // console.log(database.geActiveBike({ typeParam, nameParam, addressParam }))
  res.json(database.getActiveBike({ typeParam, nameParam, addressParam }));
});

app.post("/stations", (req, res) => {
  const station = req.body;
  database.createStations(station);
  res.status(201).end("Vehicle added successfully");
});

app.post("/vehicles", (req, res) => {
  const vehicle = req.body;
  const errorMessages = validateVehicle(vehicle);
  if (errorMessages.length === 0) {
    database.createVehicles(vehicle);
    res.status(201).end("Station added successfully");
  } else {
    res.status(403).end(errorMessages.toString());
  }
});

app.get("/check-bank-account", (req, res) => {
  const { accountNumber = 0 } = req.query;
  if (paygate.isValidBankAccountNumber(accountNumber)) {
    return res.end("Account number valid");
  }
  return res.end("Account number not existing");
});

app.get("/vehicles/renting-vehicles", (req, res) => {
  const { vehicleId = "" } = req.query;
  const result = database.searchRentingVehicles({ vehicleIdParam: vehicleId });
  res.end(JSON.stringify(result));
});

app.get("/vehicles/rent", (req, res) => {
  const { userId, vehicleId, accountNumber } = req.query;
  if (userId === undefined) {
    return res.end("userId required");
  }

  if (vehicleId === undefined) {
    return res.end("vehicleId required");
  }

  if (accountNumber === undefined) {
    return res.end("accountNumber required");
  }

  const rentedVehicleLine = { userId, vehicleId, accountNumber };
  const errorCode = validateBankAccountNumber(vehicleId, accountNumber);
  if (errorCode === -1) {
    database.createRentingVehicleLine(rentedVehicleLine);

    return res.end("201");
  }
  return res.end(String(errorCode));
});

app.listen(PORT, () => {
  console.log(`Server listening at localhost:${PORT}`);
});
