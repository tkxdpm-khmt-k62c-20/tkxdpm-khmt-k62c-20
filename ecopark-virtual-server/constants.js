const errorCodes = {
  bankAccountNumberNotExist: 1,
  bankAccountNumberUsed: 2,
  vehicleRented: 3,
  balanceNotEnough: 4,
};

module.exports = { errorCodes };
