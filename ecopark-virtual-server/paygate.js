const { errorCodes } = require("./constants");

const bankAccounts = [
  { accountNumber: "1234567890123456", balance: 0 },
  { accountNumber: "1111222233334444", balance: 100000 },
  { accountNumber: "4444333322221111", balance: 100000000 },
  { accountNumber: "1234123412341234", balance: 100000000 },
  { accountNumber: "4321432143214321", balance: 100000000 },
  { accountNumber: "9876543210987654", balance: 100000000 },
  { accountNumber: "1234567812345678", balance: 100000000 },
];

const bankAccountNumbers = bankAccounts.map(
  ({ accountNumber }) => accountNumber
);

function isValidBankAccountNumber(accountNumber) {
  return bankAccountNumbers.includes(accountNumber);
}

function getBalance(accountNumber) {
  const result = {};

  if (!isValidBankAccountNumber(accountNumber)) {
    result.errorCode = errorCodes.bankAccountNumberNotExist;
  } else {
    const accounts = bankAccounts.filter(
      ({ accountNumber: validAccountNumber }) =>
        accountNumber === validAccountNumber
    );
    result.balance = accounts[0].balance;
  }

  return result;
}

module.exports = { isValidBankAccountNumber, getBalance };
